#!/bin/bash
# $1 - start component set size
# $2 - end component set size
# $3 - step size
# $4 - number of sets per size

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# from start count to end count execute ComponentGenerator
for (( count=$1; count<=$2; count=count+$3 ))
do
  # create and change to adequate folder
  folder="$count"
  if [ ! -d $folder ]; then
    mkdir $folder
  fi
  cd $folder

  # execute runVarDroid n-times and compute average analysis time
  for (( times=0; times<$4; times++ )) 
  do
    OUTPUT_FILE="output"$times".xml"
    java -jar $BASE_DIR/ComponentGenerator.jar -config $BASE_DIR/config -o $OUTPUT_FILE -num $count
  done

  cd ..
done

