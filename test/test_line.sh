#!/bin/bash
# $1 - configuration file
# $2 - start component count
# $3 - end component count
# $4 - component count steps
# $5 - runs per component count
# $6 - temp file name

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


# if folder statistics doesn't exist, create it
# change go into folder statistics
STAT_DIR="statistics_$(basename $1)_$2_$3"
PLOT_FILE=$BASE_DIR"/"$STAT_DIR"/mean_exectimes.dat"
if [ ! -d $STAT_DIR ]; then
  mkdir $STAT_DIR
  touch $PLOT_FILE
fi
cd $STAT_DIR

# from start count to end count execute runVarDroid script
for (( count=$2; count<=$3; count=count+$4 ))
do
  # create and change to fitting folder for log files
  folder="$count"
  if [ ! -d $folder ]; then
    mkdir $folder
  fi
  cd $folder

  # execute runVarDroid n-times and compute average analysis time
  MEAN=0
  for (( times=0; times<$5; times++ )) 
  do
    echo "VarDroid execution counter: "$(($times+1))
    $BASE_DIR/runVarDroid.sh "$BASE_DIR/$1" "$BASE_DIR/blackboxes/$count/output$times.xml" $BASE_DIR"/"$6
    TIME=$(cat "$BASE_DIR/$6")
echo "$TIME"
    if [ $MEAN -eq 0 ];
      then MEAN=$TIME
      else MEAN=$(( ($MEAN+$TIME)/2 ))
    fi
  done

  echo -e "$(< $PLOT_FILE)\n$count  $MEAN" > $PLOT_FILE

  cd ..
done

# send email notification about test line finish
# create email
#to="To: hausknec@fim.uni-passau.de"
#from="From: VarDroid@fim.uni-passau.de"
#subject="Subject: VarDroid Report: Complete"

#body="Evaluation with component range from $2 to $3 completed.\n"
#body=$body"Component count steps: $4\n"
#body=$body"Runs per component count: $5\n"
#body=$body"Configuration file: $1"

#mail=$to"\n"$from"\n"$subject"\n\n"$body

#echo -e "$mail" | /usr/sbin/ssmtp hausknec@fim.uni-passau.de
