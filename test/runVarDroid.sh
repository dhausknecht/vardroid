#!/bin/bash
# $1 - config file
# $2 - component input xml file
# $3 - temp file name

BASE_DIR=$(dirname $0)

#numfiles=(*)
#numfiles=${#numfiles[@]}

#numdirs=(*/)
#numdirs=${#numdirs[@]}
#(( numfiles -= numdirs ))

help=$(echo $(basename $22))
logFile="$(date +%d_%m_%Y_%H_%M)_${help%%.*}.log"

# if component count given, generate component black boxes
#if [ "$2" ];
#  then GEN_DIR="$BASE_DIR/../ComponentGenerator"
#       java -jar $GEN_DIR/ComponentGenerator.jar -config $GEN_DIR/config -o $GEN_DIR/output.xml -num $2
#fi

# Get time as a UNIX timestamp (seconds elapsed since Jan 1, 1970 0:00 UTC)
startTime=$(date +%s%N | cut -b1-13)
startTimeStr="$(date)"

# run computation with config file from argument
java -jar -Xmx32g $BASE_DIR/VarDroid.jar $1 -input $2 | tee $logFile

endTimeStr="$(date)"
endTime=$(date +%s%N | cut -b1-13)
deltaTimeMs=$(($endTime-$startTime))
deltaTime=$(($deltaTimeMs/1000))

# create email
#to="To: hausknec@fim.uni-passau.de"
#from="From: VarDroid@fim.uni-passau.de"
#subject="Subject: VarDroid Report"

body="VarDroid execution finished ($1)"
body=$body"\n\nstart time: $startTimeStr"
body=$body"\nend time: $endTimeStr"
body=$body"\ntotal time: $((deltaTime/86400))d $((deltaTime/3600%24))h $((deltaTime/60%60))min $((deltaTime%60))s"
body=$body"\n\nRuntime output:"
body=$body"\n---------------\n\n"$(cat $logFile)
body=$body"\n\n"

#mail=$to"\n"$from"\n"$subject"\n\n"$body

# send email
#echo -e "$mail" | /usr/sbin/ssmtp hausknec@fim.uni-passau.de

# write report to file
echo -e "$body" > $logFile

# write to terminal
#echo -e "$body"

echo "$deltaTimeMs" > "$3"
