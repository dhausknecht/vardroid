package de.unipassau.vardroid;

import java.util.Calendar;

import de.unipassau.vardroid.blackbox.element.BlackBoxElement;
import de.unipassau.vardroid.conflict.ConflictDetector;
import de.unipassau.vardroid.conflict.ConflictHandler;
import de.unipassau.vardroid.conflict.ConflictParser;
import de.unipassau.vardroid.conflict.ElementConflictDetector;
import de.unipassau.vardroid.conflict.StateConflictDetector;
import de.unipassau.vardroid.connector.ElementConnector;
import de.unipassau.vardroid.connector.StateConnector;
import de.unipassau.vardroid.graph.ElementGraph;
import de.unipassau.vardroid.graph.Graph;
import de.unipassau.vardroid.graph.StateGraph;
import de.unipassau.vardroid.log.Log;
import de.unipassau.vardroid.propagation.GlobalMergePropagation;
import de.unipassau.vardroid.propagation.LevelMergePropagation;
import de.unipassau.vardroid.propagation.NoMergePropagation;
import de.unipassau.vardroid.propagation.PropagationHandler;
import de.unipassau.vardroid.propagation.VarawareGlobalMergePropagation;
import de.unipassau.vardroid.propagation.VarawareLevelMergePropagation;
import de.unipassau.vardroid.state.State;
import de.unipassau.vardroid.util.ComponentXMLParser;

/**
 * The main class for initialising and starting the variability-aware inter-component analysis.
 *
 * @author Daniel Hausknecht
 */
public class Main {
    
    public static void main(String[] args) {
        parseInput(args);
        
        // Phase 1
        BlackBoxManager bbm = phase1();

        long start = Calendar.getInstance().getTimeInMillis();
        PropagationHandler<?, ?, ?> propagationHandler = null;
        switch (Config.getConfig().getMergeMode()) {
        case Config.MERGE_MODE_NONE:
            ElementConnector noMergeConnector = new ElementConnector();
            Graph<BlackBoxElement> noMergeGraph = new ElementGraph();
            NoMergePropagation noMergePropagation = new NoMergePropagation(noMergeConnector, noMergeGraph);
            if (Config.getConfig().getConflictsXMLFilePath() != null) {
                ConflictDetector<BlackBoxElement> elemConflictDetector =
                        new ElementConflictDetector(new ConflictHandler<BlackBoxElement>());
                elemConflictDetector = ConflictParser.parseXML(Config.getConfig().getConflictsXMLFilePath(), elemConflictDetector);
                noMergePropagation.setConflictDetector(elemConflictDetector);
            } else {
                Log.w("No conflict detector specified!");
            }
            propagationHandler = noMergePropagation;
            break;
            
        case Config.MERGE_MODE_ELEM_LEVEL:
            ElementConnector levelMergeConnector = new ElementConnector();
            Graph<BlackBoxElement> levelElemGraph = new ElementGraph();
            LevelMergePropagation levelMergePropagation = new LevelMergePropagation(levelMergeConnector, levelElemGraph);
            if (Config.getConfig().getConflictsXMLFilePath() != null) {
                ConflictDetector<BlackBoxElement> elemConflictDetector =
                        new ElementConflictDetector(new ConflictHandler<BlackBoxElement>());
                elemConflictDetector = ConflictParser.parseXML(Config.getConfig().getConflictsXMLFilePath(), elemConflictDetector);
                levelMergePropagation.setConflictDetector(elemConflictDetector);
            } else {
                Log.w("No conflict detector specified!");
            }
            propagationHandler = levelMergePropagation;
            break;
            
        case Config.MERGE_MODE_ELEM_GLOBAL:
            ElementConnector globalMergeConnector = new ElementConnector();
            Graph<BlackBoxElement> globalElemGraph = new ElementGraph();
            GlobalMergePropagation globalMergePropagation = new GlobalMergePropagation(globalMergeConnector, globalElemGraph);
            if (Config.getConfig().getConflictsXMLFilePath() != null) {
                ConflictDetector<BlackBoxElement> elemConflictDetector =
                        new ElementConflictDetector(new ConflictHandler<BlackBoxElement>());
                elemConflictDetector = ConflictParser.parseXML(Config.getConfig().getConflictsXMLFilePath(), elemConflictDetector);
                globalMergePropagation.setConflictDetector(elemConflictDetector);
            } else {
                Log.w("No conflict detector specified!");
            }
            propagationHandler = globalMergePropagation;
            break;
        case Config.MERGE_MODE_STATE_LEVEL:
            StateConnector levelStateConnector = new StateConnector();
            Graph<State<? extends BlackBoxElement>> levelStateGraph = new StateGraph();
            VarawareLevelMergePropagation varawareLevelMergePropagation =
                    new VarawareLevelMergePropagation(levelStateConnector, levelStateGraph);
            if (Config.getConfig().getConflictsXMLFilePath() != null) {
                ConflictDetector<State<? extends BlackBoxElement>> stateConflictDetector =
                        new StateConflictDetector(new ConflictHandler<State<? extends BlackBoxElement>>());
                stateConflictDetector = ConflictParser.parseXML(Config.getConfig().getConflictsXMLFilePath(), stateConflictDetector);
                varawareLevelMergePropagation.setConflictDetector(stateConflictDetector);
            } else {
                Log.w("No conflict detector specified!");
            }
            propagationHandler = varawareLevelMergePropagation;
            break;
        case Config.MERGE_MODE_STATE_GLOBAL:
            StateConnector globalStateConnector = new StateConnector();
            Graph<State<? extends BlackBoxElement>> globalStateGraph = new StateGraph();
            VarawareGlobalMergePropagation varawareGlobalMergePropagation =
                    new VarawareGlobalMergePropagation(globalStateConnector, globalStateGraph);
            if (Config.getConfig().getConflictsXMLFilePath() != null) {
                ConflictDetector<State<? extends BlackBoxElement>> stateConflictDetector =
                        new StateConflictDetector(new ConflictHandler<State<? extends BlackBoxElement>>());
                stateConflictDetector = ConflictParser.parseXML(Config.getConfig().getConflictsXMLFilePath(), stateConflictDetector);
                varawareGlobalMergePropagation.setConflictDetector(stateConflictDetector);
            } else {
                Log.w("No conflict detector specified!");
            }
            propagationHandler = varawareGlobalMergePropagation;
            break;
        default:
            Log.e("Unknown state merge mode!");
            throw new IllegalStateException();
        }
        
        // execute analysis
        try {
            propagationHandler.propagateSecurityLabels(bbm, Config.getConfig().getSuccessorSearchMode());
        } catch (OutOfMemoryError E) {
            Log.e("Out of memory!!!\n");
            Log.d("\tGraph size: " + propagationHandler.getGraph().size());
            Log.d("\tTotal element count: " + propagationHandler.getGraph().totalNodeElementCount());
            Log.d("\tMin. node size: " + propagationHandler.getGraph().minNodeSize());
            Log.d("\tMax. node size: " + propagationHandler.getGraph().maxNodeSize());
            Log.d("\tMean node size: " + propagationHandler.getGraph().meanNodeSize());
        } finally {
            long end = Calendar.getInstance().getTimeInMillis();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(end - start);
            Log.d("Analysis time: " + (calendar.get(Calendar.HOUR_OF_DAY)-1) + "h "
                    + calendar.get(Calendar.MINUTE) + "min "
                    + calendar.get(Calendar.SECOND) + "s "
                    + calendar.get(Calendar.MILLISECOND) + "ms (="
                    + calendar.getTimeInMillis() + "ms)");
        }
    }
    

    /*-----------------------------------------------------------------------*
     * Initialisation                                                        *
     *-----------------------------------------------------------------------*/
    
    /**
     * Parses the program arguments and initialises the runtime configuration
     * @param args      the program arguments
     */
    private static void parseInput(String[] args) {
        Config.getConfig(args);
    }
    
    /*-----------------------------------------------------------------------*
     * PHASE 1                                                               *
     *-----------------------------------------------------------------------*/
    
    /**
     * Phase 1: Getting the component black-boxes
     */
    private static BlackBoxManager phase1() {
        BlackBoxManager bbm = null;
        switch (Config.getConfig().getInputMode()) {
        case Config.INPUT_MODE_XML:
            bbm = ComponentXMLParser.parseXML(Config.getConfig().getComponentXMLFilePath());
            break;
        default:
            System.err.println("Unknown execution mode!");
            System.exit(0);
        }
        Log.d("Phase 1 done!");
        
        return bbm;
    }
}
