package de.unipassau.vardroid;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.unipassau.vardroid.blackbox.ComponentBlackBox;
import de.unipassau.vardroid.blackbox.description.ImplicitDescription;
import de.unipassau.vardroid.blackbox.element.InputInterface;
import de.unipassau.vardroid.log.Log;

/**
 * The BlackBoxManager holds the set of all available components and can perform various operations 
 * on this set as, e.g., to find the set of launchable components.
 *
 * @author Daniel Hausknecht
 */
public class BlackBoxManager {
    private static int sInstanceCounter = 0;
    
    private Set<ComponentBlackBox> mLaunchableComponents;
    private Map<String, Set<InputInterface>> mActionMap;
//    private Map<String, InputInterface> mCategoryMap;
    private Map<String, ComponentBlackBox> mNameComponentMap; 
    
    public BlackBoxManager() {
        mLaunchableComponents = new HashSet<ComponentBlackBox>();
        mActionMap = new HashMap<String, Set<InputInterface>>();
        mNameComponentMap = new HashMap<String, ComponentBlackBox>();
    }
    
    @Override
    public String toString() {
        String result = "BlackBoxManager:\n";
        for (ComponentBlackBox cbb : mNameComponentMap.values()) {
            result += cbb.toString() + '\n';
        }
        return result;
    }
    
    public Set<ComponentBlackBox> getLaunchableComponents() {
        return mLaunchableComponents;
    }
    
    public void addComponent(ComponentBlackBox cbb) {
        if (cbb.isLaunchable()) {
            mLaunchableComponents.add(cbb);
        }
        
        if (mNameComponentMap.containsKey(cbb.getId())) {
            Log.e("Ambiguous component identifier \"" + cbb.getId() + "\"!");
            throw new IllegalStateException();
        } else {
            mNameComponentMap.put(cbb.getId(), cbb);
        }
        
        for (InputInterface input : cbb.getInputInterfaces()) {
            for (ImplicitDescription d : input.getDescriptions()) {
                for (String action : d.getActions()) {
                    Set<InputInterface> actionSet = mActionMap.get(action);
                    if (actionSet == null) {
                        actionSet = new HashSet<InputInterface>();
                        mActionMap.put(action, actionSet);
                    }
                    actionSet.add(input);
                }
            }
        }
    }
    
    /**
     * Get the component matching the Android component identifier
     * @param compName      the component identifier
     * @return              the matching component black-box
     */
    public ComponentBlackBox getComponentById(String compId) {
        return mNameComponentMap.get(compId);
    }
    
    /**
     * Get all {@link InputInterface}s satisfying the given {@link ImplicitDescription}.
     * For Android, this means all component entry points that define an intent-filter 
     * for the given implicit intent.
     * @param implDesc      the {@link ImplicitDescription} to check against
     * @return              the set of {@link InputInterface}s satisfying the {@link ImplicitDescription}.
     */
    public Set<InputInterface> getMatchingInputInterfaces(ImplicitDescription implDesc) {
        Set<InputInterface> candidates = new HashSet<InputInterface>();
        for (String action : implDesc.getActions()) {
            Set<InputInterface> inputs = mActionMap.get(action);
            if (inputs != null)
            candidates.addAll(inputs);
        }
        Set<InputInterface> result = new HashSet<InputInterface>();
        for (InputInterface input : candidates) {
            if (input.statisfiesDescription(implDesc)) {
                result.add(input);
            }
        }
        return result;
    }
    
    /**
     * Create a new instance of the given {@link ComponentBlackBox}, i.e. clone it for individual use.
     * @param original      the original {@link ComponentBlackBox}
     * @param callDepth     the component call-depth
     * @return              the cloned {@link ComponentBlackBox}
     */
    public ComponentBlackBox getNewInstance(ComponentBlackBox original, int callDepth) {
        return new ComponentBlackBox(original, ""+sInstanceCounter++, callDepth);
    }
    
    /**
     * @return      the total number of component black boxes
     */
    public int getNumberOfComponents() {
        return this.mNameComponentMap.size();
    }
}
