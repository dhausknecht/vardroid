package de.unipassau.vardroid.state;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import de.unipassau.vardroid.Config;
import de.unipassau.vardroid.blackbox.SecurityLabel;
import de.unipassau.vardroid.blackbox.element.BlackBoxElement;
import de.unipassau.vardroid.log.Log;

/**
 * TODO
 *
 * @author Daniel Hausknecht
 * @param <T>
 */
public class State<T extends BlackBoxElement> {
    // creation time call depth
    private final int mFirstCallDepth;
    // element hash -> element
    private Map<Integer, T> mHashElementMap;
    // successor elements -> successor state
    private Map<BlackBoxElement, State<? extends BlackBoxElement>> mSuccessors;
    private final Set<SecurityLabel> mSecurityLabels;
    private final int mSecurityLabelHash;
    private final String mSecurityLabelString;

    private int mHashCode = 0;
    
    public State(T element, int callDepth) {
        mFirstCallDepth = callDepth;
        mHashElementMap = new HashMap<Integer, T>();
        mHashElementMap.put(element.hashCode(), element);
        mSuccessors = new HashMap<BlackBoxElement, State<? extends BlackBoxElement>>();
        
        mSecurityLabels = new HashSet<SecurityLabel>(element.getSecurityLabels());
        mSecurityLabelHash = element.getSecurityLabelHash();
        mSecurityLabelString = element.securityLabelToString();
    }
    
    /**
     * <b>Important:</b> the hash code can only be update whenever the state itself changes, 
     * not when the black box elements within the state change. There is no callback or anything.
     * So, NEVER FUCKING CHANGE A BLACK BOX ELEMENT WITHIN A STATE!!!11eleven
     */
    @Override
    public int hashCode() {
        if (mHashCode == 0) {
            HashCodeBuilder builder = new HashCodeBuilder(17, 31);
            builder.append(mSecurityLabelHash);
            for (T elem : mHashElementMap.values()) {
                builder.append(elem.hashCode());
            }
            // a cleaner implementation would do the following lines in the fitting PropagationHandler
            // but hey, it works this way. When you're bored and can't sleep, help yourself and change it!
            if (Config.getConfig().getMergeMode() == Config.MERGE_MODE_STATE_LEVEL) {
                builder.append(Config.getConfig().getCurrentCallDepth());
            }
            mHashCode = builder.toHashCode();
        }
        return mHashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof State<?>) {
            State<?> state = (State<?>) obj;
            Iterator<T> i1 = this.getMergedElements().iterator();
            Iterator<?> i2 = state.getMergedElements().iterator();
            // check if both states have same 'T'. Cause Java is weird you cannot check this by simply 
            // saying "if T1==T2)"
            if (i1.hasNext() && i2.hasNext() && (i1.next().getClass().equals(i2.next().getClass()))) {
                return (this.hashCode() == state.hashCode());
            }
        }
        return false;
    }
    
    public Collection<T> getMergedElements() {
        return mHashElementMap.values();
    }
    
    /**
     * @return      the creation time call depth
     */
    public int getFirstCallDepth() {
        return mFirstCallDepth;
    }
    
    /**
     * TODO
     * @param hash
     * @return
     */
    public T getMergedElement(int hash) {
        return mHashElementMap.get(hash);
    }
    
    /**
     * TODO
     * @param element
     * @param predElem
     * @param callDepth
     */
    public void addMergedElement(T element) {
        mHashElementMap.put(element.hashCode(), element);
        mHashCode = 0;
    }
    
    /**
     * TODO
     * @param element
     * @param predElem
     * @param callDepth
     */
    public void addMergedElement(T element, int callDepth) {
        T mergedElement = mHashElementMap.get(element.hashCode());
        if (mergedElement == null) {
            mHashElementMap.put(element.hashCode(), element);
        }
        mHashCode = 0;
    }
    
    public void removeMergedElement(T element) {
        mHashCode = 0;
        T mergedElement = mHashElementMap.get(element.hashCode());
        if (mergedElement == null) {
            Log.e("No black-box element \"" + element.toString() + "\" found in current state!");
            throw new NullPointerException();
        } else {
            mHashElementMap.remove(element.hashCode());
        }
    }
    
    public Map<BlackBoxElement, State<? extends BlackBoxElement>> getSuccessors() {
        return mSuccessors;
    }
    
    public void addSuccessor(State<? extends BlackBoxElement> state, BlackBoxElement element) {
        mSuccessors.put(element, state);
    }
    
    public Set<SecurityLabel> getSecurityLabels() {
        return mSecurityLabels;
    }
    
    public int getSecurityLabelHash() {
        return mSecurityLabelHash;
    }
    
    public String securityLabelToString() {
        return mSecurityLabelString;
    }
    
    public boolean isEmpty() {
        return mHashElementMap.isEmpty();
    }
    
    @Override
    public String toString() {
        String result = "";
        boolean secLabelPrinted = false;
        for (T elem : mHashElementMap.values()) {
            if (!secLabelPrinted) {
                result = securityLabelToString();
                secLabelPrinted = true;
            }
            result += ", " + elem.getComponent().getType() + "." + elem.getId();
        }
        if (result.equals("")) { result = "EMPTY"; }
        return result + ", call depth: " + getFirstCallDepth();
    }
}