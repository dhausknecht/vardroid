package de.unipassau.vardroid.connector;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;

import de.unipassau.vardroid.BlackBoxManager;
import de.unipassau.vardroid.Config;
import de.unipassau.vardroid.blackbox.ComponentBlackBox;
import de.unipassau.vardroid.blackbox.description.Description;
import de.unipassau.vardroid.blackbox.description.ExplicitDescription;
import de.unipassau.vardroid.blackbox.description.ImplicitDescription;
import de.unipassau.vardroid.blackbox.element.InputInterface;
import de.unipassau.vardroid.blackbox.element.OutputInterface;
import de.unipassau.vardroid.state.State;

public class StateConnector extends ComponentConnector<State<InputInterface>, State<OutputInterface>> {

    @Override
    public Collection<State<InputInterface>> getLaunchableInputInterfaces(BlackBoxManager bbm) {
        Collection<State<InputInterface>> startInputInterfaces = new LinkedList<State<InputInterface>>();
        
        // find all InputInterfaces used on component launch
        for (ComponentBlackBox cbb : bbm.getLaunchableComponents()) {
            ComponentBlackBox startComp = bbm.getNewInstance(cbb, 0);
            if (startComp.isLaunchable()) {
                startInputInterfaces.add(new State<InputInterface>(startComp.getLaunchableInputInterface(), 0));
            }
        }
        
        return startInputInterfaces;
    }

    @Override
    public Collection<State<InputInterface>> createComponentCallGraph(BlackBoxManager bbm) {
        Collection<State<InputInterface>> startInputInterfaces = new LinkedList<State<InputInterface>>();
        Collection<InputInterface> inputs = new LinkedList<InputInterface>();
        
        // find all InputInterfaces used on component launch
        for (ComponentBlackBox cbb : bbm.getLaunchableComponents()) {
            ComponentBlackBox startComp = bbm.getNewInstance(cbb, 0);
            if (startComp.isLaunchable()) {
                startInputInterfaces.add(new State<InputInterface>(startComp.getLaunchableInputInterface(), 0));
                inputs.add(startComp.getLaunchableInputInterface());
            }
        }
        
        // connect all OutputIntefaces with the next reachable InputInterfaces
        while (Config.getConfig().getCurrentCallDepth() < Config.getConfig().getMaxAnalysisDepth()) {
            inputs = findSuccessors(inputs, bbm);
            Config.getConfig().increaseCurrentCallDepth();
        }
        
        return startInputInterfaces;
    }

    @Override
    public Collection<State<OutputInterface>> findInputInterfaces(Collection<State<OutputInterface>> outputs, BlackBoxManager bbm) {
        for (State<OutputInterface> state : outputs) {
            for (OutputInterface out : state.getMergedElements()) {
                Description descr = out.getDescription();
                if (descr instanceof ExplicitDescription) {
                    ExplicitDescription explIntent = (ExplicitDescription) descr;
                    ComponentBlackBox cbb = bbm.getComponentById(explIntent.getComponentName());
                    // check permission before connecting
                    if (out.getComponent().isPermissionGranted(cbb.getStartupInterface())) {
                        cbb = bbm.getNewInstance(cbb, out.getComponent().getCallDepth()+1);
                        out.addTargetInputInterface(cbb.getStartupInterface());
                        Config.getConfig().getGraphvizWriter(Config.DOT_PHASE2_PATH).addTransition(out.toString(), cbb.getStartupInterface().toString());
                    }
                    
                } else if (descr instanceof ImplicitDescription) {
                    ImplicitDescription implIntent = (ImplicitDescription) descr;
                    Set<InputInterface> matches = bbm.getMatchingInputInterfaces(implIntent);
                    for (InputInterface i : matches) {
                        // check permission before connecting
                        if (out.getComponent().isPermissionGranted(i)) {
                            ComponentBlackBox cbb = bbm.getNewInstance(i.getComponent(), out.getComponent().getCallDepth()+1);
                            InputInterface input = cbb.getInputInterface(i.getId());
                            out.addTargetInputInterface(input);
                            Config.getConfig().getGraphvizWriter(Config.DOT_PHASE2_PATH).addTransition(out.toString(), input.toString());
                        }
                    }
                }
            }
        }
    return outputs;
    }

}
