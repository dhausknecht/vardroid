/**
 * 
 */
package de.unipassau.vardroid.connector;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import de.unipassau.vardroid.BlackBoxManager;
import de.unipassau.vardroid.Config;
import de.unipassau.vardroid.blackbox.ComponentBlackBox;
import de.unipassau.vardroid.blackbox.description.Description;
import de.unipassau.vardroid.blackbox.description.ExplicitDescription;
import de.unipassau.vardroid.blackbox.description.ImplicitDescription;
import de.unipassau.vardroid.blackbox.element.DataSink;
import de.unipassau.vardroid.blackbox.element.DataSource;
import de.unipassau.vardroid.blackbox.element.InputInterface;
import de.unipassau.vardroid.blackbox.element.InternalSource;
import de.unipassau.vardroid.blackbox.element.OutputInterface;

/**
 * Interface for the functionality of Phase 2, i.e. the connection of component interfaces.
 * There are two basic modes: either to create the whole call-graph before propagating security labels, 
 * or to find the successive components on demand. The latter is for integrating Phase 2 in Phase 3. 
 *
 * @author Daniel Hausknecht
 */
public abstract class ComponentConnector<IN, OUT> {
    
    /**
     * Get all input-interfaces which are called on the respective application launches.
     * @param bbm       the {@link BlackBoxManager} holding the available components
     * @return          a {@link Collection} of all launchable input-interfaces
     */
    public abstract Collection<IN> getLaunchableInputInterfaces(BlackBoxManager bbm);
    
    /**
     * Create the full component call-graph based on the components in the {@link BlackBoxManager}.
     * @param bbm       the {@link BlackBoxManager} holding the available components
     * @return          the {@link Collection} of input-interfaces which are the starting nodes of the graph
     */
    public abstract Collection<IN> createComponentCallGraph(BlackBoxManager bbm);
    
    /**
     * Find all from a {@link Collection} of output-interfaces reachable input-interfaces and update 
     * the successor set of each output-interface accordingly. This method is meant to be used when 
     * integrating Phase 2 in Phase 3.
     * @param outputs       the {@link Collection} of currently reached output-interfaces
     * @param bbm           the {@link BlackBoxManager} holding the available components
     * @return              the {@link Collection} of updated output-interfaces
     */
    public abstract Collection<OUT> findInputInterfaces(Collection<OUT> outputs, BlackBoxManager bbm);

    
    /**
     * For each given {@link InputInterface}, find all {@link InputInterface}s which are directly 
     * reachable from the {@link OutputInterface}s of the {@link ComponentBlackBox} the 
     * {@link InputInterface} is part of. Also, connect all the {@link OutputInterface}s with the 
     * newly found {@link InputInterface}s.
     * @param inputs    the initial set of {@link InputInterface}s
     * @param bbm       the {@link BlackBoxManager} holding the raw {@link ComponentBlackBox}es
     */
    protected final Collection<InputInterface> findSuccessors(Collection<InputInterface> inputs, BlackBoxManager bbm) {
        Collection<InputInterface> succInputs = new LinkedList<InputInterface>();
        
        for (InputInterface i : inputs) {
            ComponentBlackBox cbb = i.getComponent();
            Collection<DataSource> sources = new LinkedList<DataSource>(); 
            sources.addAll(cbb.getInternalSources());
            // draw transitions for dot files
            if (Config.getConfig().printDotFile()) {
                for (InternalSource intSource : cbb.getInternalSources()) {
                    Config.getConfig().getGraphvizWriter(Config.DOT_PHASE2_PATH).addDashedTransition(i.toString(), intSource.toString());
                }
            }
            sources.add(i);
            for (DataSource s : sources) {
                for (DataSink sink : s.getSinks()) {
                    // draw transitions for dot files
                    Config.getConfig().getGraphvizWriter(Config.DOT_PHASE2_PATH).addTransition(s.toString(), sink.toString());
                    if (sink instanceof OutputInterface) {
                        OutputInterface output = (OutputInterface) sink;
                        Set<InputInterface> successors = findInputInterfaces(output, bbm);
                        output.setTargetInputInterfaces(successors);
                        // TODO check for granted permissions!
                        succInputs.addAll(successors);
                    }
                }
            }
        }
        
        return succInputs;
    }
    
    /**
     * Find all {@link InputInterface}s which can be directly reached from the given {@link OutputInterface}.
     * For Android, this means all component entry points that can be called via an intent from the 
     * given component exit point.
     * @param output        the {@link OutputInterface}
     * @return              the set of {@link InputInterface}s directly reachable from the {@link OutputInterface}
     */
    private final Set<InputInterface> findInputInterfaces(OutputInterface output, BlackBoxManager bbm) {
        Set<InputInterface> inputs = new HashSet<InputInterface>();
        
        Description descr = output.getDescription();
        if (descr instanceof ExplicitDescription) {
            ExplicitDescription explIntent = (ExplicitDescription) descr;
            ComponentBlackBox cbb = bbm.getComponentById(explIntent.getComponentName());
            cbb = bbm.getNewInstance(cbb, output.getComponent().getCallDepth()+1);
            inputs.add(cbb.getStartupInterface());
            Config.getConfig().getGraphvizWriter(Config.DOT_PHASE2_PATH).addTransition(output.toString(), cbb.getStartupInterface().toString());
            
        } else if (descr instanceof ImplicitDescription) {
            ImplicitDescription implIntent = (ImplicitDescription) descr;
            Set<InputInterface> matches = bbm.getMatchingInputInterfaces(implIntent);
            for (InputInterface i : matches) {
                ComponentBlackBox cbb = bbm.getNewInstance(i.getComponent(), output.getComponent().getCallDepth()+1);
                InputInterface input = cbb.getInputInterface(i.getId());
                inputs.add(input);
                Config.getConfig().getGraphvizWriter(Config.DOT_PHASE2_PATH).addTransition(output.toString(), input.toString());
            }
        }
        return inputs;
    }
}
