/**
 * 
 */
package de.unipassau.vardroid.connector;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import de.unipassau.vardroid.BlackBoxManager;
import de.unipassau.vardroid.Config;
import de.unipassau.vardroid.blackbox.ComponentBlackBox;
import de.unipassau.vardroid.blackbox.description.Description;
import de.unipassau.vardroid.blackbox.description.ExplicitDescription;
import de.unipassau.vardroid.blackbox.description.ImplicitDescription;
import de.unipassau.vardroid.blackbox.element.InputInterface;
import de.unipassau.vardroid.blackbox.element.OutputInterface;

/**
 * TODO
 *
 * @author Daniel Hausknecht
 */
public class ElementConnector extends ComponentConnector<InputInterface, OutputInterface> {

    @Override
    public Collection<InputInterface> getLaunchableInputInterfaces(BlackBoxManager bbm) {
        Set<InputInterface> startInputInterfaces = new HashSet<InputInterface>();
        
        // find all InputInterfaces used on component launch
        for (ComponentBlackBox cbb : bbm.getLaunchableComponents()) {
            ComponentBlackBox startComp = bbm.getNewInstance(cbb, 0);
            if (startComp.isLaunchable()) {
                startInputInterfaces.add(startComp.getLaunchableInputInterface());
            }
        }
        
        return startInputInterfaces;
    }

    @Override
    public Collection<InputInterface> createComponentCallGraph(BlackBoxManager bbm) {
        Collection<InputInterface> startInputs = getLaunchableInputInterfaces(bbm);
        
        // connect all OutputIntefaces with the next reachable InputInterfaces
        Collection<InputInterface> inputs = startInputs;
        while (Config.getConfig().getCurrentCallDepth() < Config.getConfig().getMaxAnalysisDepth()) {
            inputs = findSuccessors(inputs, bbm);
            Config.getConfig().increaseCurrentCallDepth();
        }
        
        return startInputs;
    }

    @Override
    public Collection<OutputInterface> findInputInterfaces(Collection<OutputInterface> outputs, BlackBoxManager bbm) {
        for (OutputInterface out : outputs) {
            Description descr = out.getDescription();
            if (descr instanceof ExplicitDescription) {
                ExplicitDescription explIntent = (ExplicitDescription) descr;
                ComponentBlackBox cbb = bbm.getComponentById(explIntent.getComponentName());
                // check permission before connecting
                if (out.getComponent().isPermissionGranted(cbb.getStartupInterface())) {
                    cbb = bbm.getNewInstance(cbb, out.getComponent().getCallDepth()+1);
                    out.addTargetInputInterface(cbb.getStartupInterface());
                    Config.getConfig().getGraphvizWriter(Config.DOT_PHASE2_PATH).addTransition(out.toString(), cbb.getStartupInterface().toString());
                }
                
            } else if (descr instanceof ImplicitDescription) {
                ImplicitDescription implIntent = (ImplicitDescription) descr;
                Set<InputInterface> matches = bbm.getMatchingInputInterfaces(implIntent);
                for (InputInterface i : matches) {
                    // check permission before connecting
                    if (out.getComponent().isPermissionGranted(i)) {
                        ComponentBlackBox cbb = bbm.getNewInstance(i.getComponent(), out.getComponent().getCallDepth()+1);
                        InputInterface input = cbb.getInputInterface(i.getId());
                        out.addTargetInputInterface(input);
                        Config.getConfig().getGraphvizWriter(Config.DOT_PHASE2_PATH).addTransition(out.toString(), input.toString());
                    }
                }
            }
        }
        
        return outputs;
    }

}
