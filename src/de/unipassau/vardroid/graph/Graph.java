package de.unipassau.vardroid.graph;

import java.util.Collection;
import java.util.HashMap;

import de.unipassau.vardroid.blackbox.element.BlackBoxElement;
import de.unipassau.vardroid.log.Log;
import de.unipassau.vardroid.state.State;

/**
 * The generic graph representation. Its purpose is mostly to hide the actual way the nodes are stored, 
 * i.e. whether the nodes are e.g. kept in RAM or added to a database.
 *
 * @author Daniel Hausknecht
 * @param <T>       the node type. For VarDroid it is expected to use either {@link BlackBoxElement} or {@link State}.
 */
public abstract class Graph<T> {
    private HashMap<T, T> mNodeMap;
    
    protected double mMeanNodeSize = -1;
    protected int mTotalNodeElementCount = -1;
    protected int mMinNodeSize = -1;
    protected int mMaxNodeSize = -1;
    
    public Graph() {
        mNodeMap = new HashMap<T, T>();
    }
    
    /**
     * Insert the node to the graph structure
     * @param node      the graph node
     */
    public void insert(T node) {
        T oldNode = mNodeMap.put(node, node);
        if (oldNode != null) {
            Log.w("Collision in graph: " + oldNode.toString());
        }
    }
    
    public void insertAll(Collection<? extends T> nodes) {
        for (T node : nodes) { insert(node); }
    }
    
    /**
     * @param key       the equal node (i.e. it must not be the exact same object!)
     * @return          <code>true</code> if the graph contains the node key,
     *                  <code>false</code> otherwise
     */
    public boolean contains(T key) {
        return mNodeMap.containsKey(key);
    }
    
    /**
     * @param key      the equal node (i.e. it must not be the exact same object!)
     * @return         the equal node which is already in the set
     */
    public T getNode(T key) {
        return mNodeMap.get(key);
    }
    
    public Collection<T> nodes() {
        return mNodeMap.values();
    }
    
    public int size() {
        return mNodeMap.size();
    }
    
    /**
     * Get the mean number of elements merged to the single node
     * @return      the mean number
     */
    public abstract double meanNodeSize();
    
    public abstract int totalNodeElementCount();

    public abstract int minNodeSize();
    
    public abstract int maxNodeSize();
    
    public abstract void draw();
}