package de.unipassau.vardroid.graph;

import de.unipassau.vardroid.Config;
import de.unipassau.vardroid.blackbox.element.BlackBoxElement;
import de.unipassau.vardroid.blackbox.element.InputInterface;
import de.unipassau.vardroid.blackbox.element.InternalSink;
import de.unipassau.vardroid.blackbox.element.InternalSource;
import de.unipassau.vardroid.blackbox.element.OutputInterface;
import de.unipassau.vardroid.log.GraphvizWriter;
import de.unipassau.vardroid.state.State;

public class StateGraph extends Graph<State<? extends BlackBoxElement>> {
    
    public StateGraph() {
        super();
    }

    @Override
    public double meanNodeSize() {
        if (mMeanNodeSize < 0) {
            createStatistics();
        }
        return mMeanNodeSize;
    }

    @Override
    public int totalNodeElementCount() {
        if (mTotalNodeElementCount < 0) {
            createStatistics();
        }
        return mTotalNodeElementCount;
    }

    @Override
    public int minNodeSize() {
        if (mMinNodeSize < 0) {
            createStatistics();
        }
        return mMinNodeSize;
    }

    @Override
    public int maxNodeSize() {
        if (mMaxNodeSize < 0) {
            createStatistics();
        }
        return mMaxNodeSize;
    }
    
    private void createStatistics() {
        mTotalNodeElementCount = 0;
        mMaxNodeSize = 0;
        mMinNodeSize = Integer.MAX_VALUE;
        for (State<? extends BlackBoxElement> node : nodes()) {
            int nodeSize = node.getMergedElements().size();
            mMeanNodeSize = (mMeanNodeSize < 0) ? nodeSize : ((mMeanNodeSize + nodeSize) / 2);
            mTotalNodeElementCount += nodeSize;
            mMinNodeSize = (nodeSize < mMinNodeSize) ? nodeSize : mMinNodeSize;
            mMaxNodeSize = (mMaxNodeSize < nodeSize) ? nodeSize : mMaxNodeSize;
        }
        if (mMinNodeSize == Integer.MAX_VALUE) { mMinNodeSize = 0; }
    }

    @Override
    public void draw() {
        GraphvizWriter gw = Config.getConfig().getGraphvizWriter(Config.DOT_PHASE3_STATE_PATH);
        for (State<? extends BlackBoxElement> node : nodes()) {
            String startNode = node.securityLabelToString() + "_" + node.getFirstCallDepth();
            for (BlackBoxElement elem : node.getMergedElements()) {
                if (elem instanceof InputInterface) {
                    startNode = "IN_" + startNode;
                } else if (elem instanceof InternalSource) {
                    startNode = "SRC_" + startNode;
                } else if (elem instanceof InternalSink) {
                    startNode = "SINK_" + startNode;
                } else if (elem instanceof OutputInterface) {
                    startNode = "OUT_" + startNode;
                }
                break;
            }
            for (BlackBoxElement succElem : node.getSuccessors().keySet()) {
                State<? extends BlackBoxElement> targetState = node.getSuccessors().get(succElem);
                String endNode = targetState.securityLabelToString() + "_" + targetState.getFirstCallDepth();
                if (succElem instanceof InputInterface) {
                    endNode = "IN_" + endNode;
                } else if (succElem instanceof InternalSource) {
                    endNode = "SRC_" + endNode;
                } else if (succElem instanceof InternalSink) {
                    endNode = "SINK_" + endNode;
                } else if (succElem instanceof OutputInterface) {
                    endNode = "OUT_" + endNode;
                }
                gw.addTransition(startNode, endNode);
            }
        }
        
        Config.getConfig().finaliseGraphvizWriter(Config.DOT_PHASE3_STATE_PATH);
    }

}
