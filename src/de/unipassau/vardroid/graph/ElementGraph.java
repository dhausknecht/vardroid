package de.unipassau.vardroid.graph;

import de.unipassau.vardroid.Config;
import de.unipassau.vardroid.blackbox.element.BlackBoxElement;
import de.unipassau.vardroid.blackbox.element.DataSink;
import de.unipassau.vardroid.blackbox.element.InputInterface;
import de.unipassau.vardroid.blackbox.element.InternalSink;
import de.unipassau.vardroid.blackbox.element.InternalSource;
import de.unipassau.vardroid.blackbox.element.OutputInterface;
import de.unipassau.vardroid.log.GraphvizWriter;

public class ElementGraph extends Graph<BlackBoxElement> {
    
    public ElementGraph() {
        super();
    }

    @Override
    public double meanNodeSize() {
        return 1;
    }

    @Override
    public int totalNodeElementCount() {
        return this.size();
    }

    @Override
    public int minNodeSize() {
        return 1;
    }

    @Override
    public int maxNodeSize() {
        return 1;
    }

    @Override
    public void draw() {
        GraphvizWriter gw = Config.getConfig().getGraphvizWriter(Config.DOT_PHASE3_PATH);
        for (BlackBoxElement node : nodes()) {
            String startNode = node.getComponent().getType() + "_" + node.getId() + "_" + node.securityLabelToString()
                    + "_" + node.getComponent().getCallDepth();
            if (node instanceof InputInterface) {
                startNode = "IN_" + startNode;
                InputInterface in = (InputInterface) node;
                for (InternalSource is : in.getComponent().getInternalSources()) {
                    String endNode = "SRC_" + is.getComponent().getType() + "_" + is.getId()
                            + "_" + is.securityLabelToString() + "_" + getNode(is).getComponent().getCallDepth();
                    gw.addDashedTransition(startNode, endNode);
                }
                for (DataSink succElem : in.getSinks()) {
                    String endNode = succElem.getComponent().getType() + "_" + succElem.getId()
                            + "_" + succElem.securityLabelToString() + "_" + succElem.getComponent().getCallDepth();
                    if (succElem instanceof InternalSink) {
                        endNode = "SINK_" + endNode;
                    } else if (succElem instanceof OutputInterface) {
                        endNode = "OUT_" + endNode;
                    }
                    gw.addTransition(startNode, endNode);
                }
            } else if (node instanceof InternalSource) {
                startNode = "SRC_" + startNode;
                InternalSource is = (InternalSource) node;
                for (DataSink succElem : is.getSinks()) {
                    String endNode = succElem.getComponent().getType() + "_" + succElem.getId()
                            + "_" + succElem.securityLabelToString() + "_" + succElem.getComponent().getCallDepth();
                    if (succElem instanceof InternalSink) {
                        endNode = "SINK_" + endNode;
                    } else if (succElem instanceof OutputInterface) {
                        endNode = "OUT_" + endNode;
                    }
                    gw.addTransition(startNode, endNode);
                }
//            } else if (elem instanceof InternalSink) {
//                startNode = "SINK_" + startNode;
            } else if (node instanceof OutputInterface) {
                startNode = "OUT_" + startNode;
                OutputInterface is = (OutputInterface) node;
                for (InputInterface succElem : is.getTargetInputInterfaces()) {
                    String endNode = "IN_" + succElem.getComponent().getType() + "_" + succElem.getId()
                            + "_" + succElem.securityLabelToString() + "_" + succElem.getComponent().getCallDepth();
                    gw.addTransition(startNode, endNode);
                }
            }
        }
        
        Config.getConfig().finaliseGraphvizWriter(Config.DOT_PHASE3_PATH);
    }

}
