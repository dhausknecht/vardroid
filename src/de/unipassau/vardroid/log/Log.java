package de.unipassau.vardroid.log;

import de.unipassau.vardroid.Config;

/**
 * The logger to control where log output goes to.
 *
 * @author Daniel Hausknecht
 */
public final class Log {
    
    /**
     * Output a debug message to the logger.
     * @param msg       the message
     */
    public static void d(String msg) {
        if (Config.getConfig().printDebugLog()) {
            System.out.println("D - " + msg);
        }
    }

    /**
     * Output a warning message to the logger.
     * @param msg       the message
     */
    public static void w(String msg) {
        if (Config.getConfig().printWarningLog()) {
            System.out.println("W - " + msg);
        }
    }

    /**
     * Output an error message to the logger.
     * @param msg       the message
     */
    public static void e(String msg) {
        if (Config.getConfig().printErrorLog()) {
            System.out.println("ERROR - " + msg);
        }
    }
}
