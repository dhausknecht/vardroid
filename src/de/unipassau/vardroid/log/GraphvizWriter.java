package de.unipassau.vardroid.log;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import de.unipassau.vardroid.Config;

public class GraphvizWriter {
    private String mFilePath;
    private PrintWriter mPrintWriter;
    
//    public GraphvizWriter() { }
    
    public GraphvizWriter(String filePath) {
        mFilePath = filePath;
        openFile();
    }
    
    private void openFile() {
        try {
            mPrintWriter = new PrintWriter(mFilePath);
            mPrintWriter.println("digraph G {");
        } catch (FileNotFoundException e) {
            mPrintWriter = null;
            Log.e("Failed to open File \""+ mFilePath + "\"");
        }
    }
    
    public void addTransition(String start, String end) {
        if ((mPrintWriter != null) && Config.getConfig().printDotFile()) {
            addTransition(start, end, null);
        }
    }
    
    public void addDashedTransition(String start, String end) {
        if ((mPrintWriter != null) && Config.getConfig().printDotFile()) {
            addDashedTransition(start, end, null);
        }
    }
    
    public void addTransition(String start, String end, String label) {
        if ((mPrintWriter != null) && Config.getConfig().printDotFile()) {
            if ((label == null) || (!Config.getConfig().hasTransitionLabels())) {
                mPrintWriter.println(start + " -> " + end);
            } else {
                mPrintWriter.println(start + " -> " + end + " [label=\"[" + label + "]\"]");
            }
        }
    }
    
    public void addDashedTransition(String start, String end, String label) {
        if ((mPrintWriter != null) && Config.getConfig().printDotFile()) {
            if ((label == null)  || (!Config.getConfig().hasTransitionLabels())) {
                mPrintWriter.println(start + " -> " + end + " [style=dashed]");
            } else {
                mPrintWriter.println(start + " -> " + end + " [style=dashed, label=\"[" + label + "]\"]");
            }
        }
    }
    
    public void closeFile() {
        if (mPrintWriter != null) {
            mPrintWriter.println("}");
            mPrintWriter.close();
        }
    }
}
