package de.unipassau.vardroid.blackbox.element;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import de.unipassau.vardroid.blackbox.ComponentBlackBox;
import de.unipassau.vardroid.blackbox.Permission;
import de.unipassau.vardroid.blackbox.description.Description;

/**
 * Interface for a component output-interface as part of a component black-box.
 *
 * @author Daniel Hausknecht
 */
public class OutputInterface extends DataSink {
    private Description mDescription;
    private Set<Permission> mPermissions;
    private Collection<InputInterface> mTargetInputInterfaces;
    
    /**
     * Create an {@link OutputInterface} instance
     * @param id        the {@link OutputInterface} identifier
     * @param cbb       the {@link ComponentBlackBox} the input-interface is part of
     */
    public OutputInterface(String id, ComponentBlackBox cbb) {
        super(id, cbb);
        mPermissions =  new HashSet<Permission>();
        mTargetInputInterfaces =  new LinkedList<InputInterface>();
    }
    
    /**
     * Copy constructor for a new {@link OutputInterface} (partially deep copy)
     * @param id            the {@link OutputInterface} identifier
     * @param cbb           the {@link ComponentBlackBox} the {@link InputInterface} is part of
     * @param original      the {@link OutputInterface} to copy
     */
    public OutputInterface(String id, ComponentBlackBox cbb, OutputInterface original) {
        super(id, cbb, original);
        mDescription = original.getDescription();   // never changes, so no deep copy necessary
        mPermissions = original.getPermissions();   // never changes, so no deep copy necessary
        mTargetInputInterfaces = new LinkedList<InputInterface>();
        for (InputInterface input : original.getTargetInputInterfaces()) {
            mTargetInputInterfaces.add(new InputInterface(input.getId(), cbb, input));
        }
    }
    
    @Override
    public int hashCode() {
        if (mHashCode == 0) {
            HashCodeBuilder builder = new HashCodeBuilder(17, 31).append(getComponent().hashCode()).append(getId());
            builder.append(getSecurityLabelHash());
//            int targets = 0;
//            for (InputInterface input : this.getTargetInputInterfaces()) {
//                targets += (input.getComponent().getType()+input.getId()).hashCode();
//            }
//            builder.append(targets);
            mHashCode = builder.toHashCode();
        }
        return mHashCode;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof OutputInterface) {
            return super.equals(o);
        }
        return false;
    }

    /**
     * The descriptions of the target input-interfaces
     * @return      the set of description
     */
    public Description getDescription() {
        return mDescription;
    }

    public void setDescription(Description description) {
        mDescription = description;
        mHashCode = 0;
    }
    
    /**
     * The permissions granted to the component
     * @return  the required permissions
     */
    public Set<Permission> getPermissions() {
        return mPermissions;
    }

    /**
     * Set the {@link InputInterface}s that can be directly reached from the {@link OutputInterface}
     * @param successors    the reachable {@link InputInterface}s
     */
    public void setTargetInputInterfaces(Set<InputInterface> successors) {
        mTargetInputInterfaces = successors;
        mHashCode = 0;
    }

    /**
     * @return the directly reachable InputInterfaces
     */
    public Collection<InputInterface> getTargetInputInterfaces() {
        return mTargetInputInterfaces;
    }
    
    public void addTargetInputInterface(InputInterface input) {
        mTargetInputInterfaces.add(input);
        mHashCode = 0;
    }
    
    public void removeTargetInputInterface(InputInterface input) {
        mTargetInputInterfaces.remove(input);
        mHashCode = 0;
    }
}
