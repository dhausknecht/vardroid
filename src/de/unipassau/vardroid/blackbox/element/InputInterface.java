package de.unipassau.vardroid.blackbox.element;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import de.unipassau.vardroid.blackbox.ComponentBlackBox;
import de.unipassau.vardroid.blackbox.Permission;
import de.unipassau.vardroid.blackbox.description.Description;
import de.unipassau.vardroid.blackbox.description.ImplicitDescription;

/**
 * Interface for a component input-interface as part of a component black-box.
 *
 * @author Daniel Hausknecht
 */
public class InputInterface extends DataSource {
    private Set<ImplicitDescription> mDescriptions;
    private Set<Permission> mPermissions;
    
    /**
     * Create an {@link InputInterface} instance
     * @param id        the {@link InputInterface} identifier
     * @param cbb       the {@link ComponentBlackBox} the input-interface is part of
     */
    public InputInterface(String id, ComponentBlackBox cbb) {
        super(id, cbb);
        mDescriptions = new HashSet<ImplicitDescription>();
        mPermissions =  new HashSet<Permission>();
    }
    
    /**
     * Copy constructor for a new {@link InputInterface} (shallow copy)
     * @param id            the {@link InputInterface} identifier
     * @param cbb           the {@link ComponentBlackBox} the {@link InputInterface} is part of
     * @param original      the {@link InputInterface} to copy
     */
    public InputInterface(String id, ComponentBlackBox cbb, InputInterface original) {
        super(id, cbb, original);
        mDescriptions = original.getDescriptions();   // never changes, so no deep copy necessary
        mPermissions = original.getPermissions();   // never changes, so no deep copy necessary
    }
    
    @Override
    public int hashCode() {
        if (mHashCode == 0) {
            HashCodeBuilder builder = new HashCodeBuilder(17, 31).append(getComponent().hashCode()).append(getId().hashCode());
            builder.append(getSecurityLabelHash());
            mHashCode = builder.toHashCode();
        }
        return mHashCode;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof InputInterface) {
            return super.equals(o);
        }
        return false;
    }
    
    /**
     * The description of which service is provided by the component accessible through the input-interface
     * @return      the input-interface descriptions
     */
    public Set<ImplicitDescription> getDescriptions() {
        return mDescriptions;
    }

    public void addDescription(ImplicitDescription description) {
        mDescriptions.add(description);
        mHashCode = 0;
    }
    
    /**
     * The permissions required to access the component via the input-interface
     * @return  the required permissions
     */
    public Set<Permission> getPermissions() {
        return mPermissions;
    }
    
    public void addRequiredPermission(Permission p) {
        mPermissions.add(p);
    }
    
    /**
     * Check whether the {@link Description} of the {@link InputInterface} matches the given 
     * {@link Description}. For Android, this means whether the intent-filter of the input-interface 
     * suffices for the given implicit intent.  
     * @param impDesc   the implicit description to check against
     * @return          <code>true</code> if the objects description satisfies the given description,
     *                  <code>false</code> otherwise
     */
    public boolean statisfiesDescription(ImplicitDescription impDesc) {
        // it is assumed that an input-interface's description is an intent-filter and thus an implicit description
        for (ImplicitDescription d : mDescriptions) {
            if (d.satisfiesIntent(impDesc)) {
                return true;
            }
        }
        return false;
    }
}
