package de.unipassau.vardroid.blackbox.element;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import de.unipassau.vardroid.blackbox.ComponentBlackBox;

/**
 * Interface for a component-internal data source as part of a component black-box.
 *
 * @author Daniel Hausknecht
 */
public class InternalSource extends DataSource {
    
    /**
     * Construct a new {@link InternalSource} object
     * @param id        the {@link InternalSource} identifier
     * @param cbb
     */
    public InternalSource(String id, ComponentBlackBox cbb) {
        super(id, cbb);
    }
    
    /**
     * Copy constructor for a new {@link InternalSource} instance (deep copy)
     * @param id        the {@link InternalSource} identifier
     * @param cbb
     * @param original      the {@link InternalSource} to copy
     */
    public InternalSource(String id, ComponentBlackBox cbb, InternalSource original) {
        super(id, cbb, original);
    }
    
    public InternalSource mergeWith(InternalSource is) {
        this.addSuccessors(is.getSinks());
        return this;
    }
    
    @Override
    public int hashCode() {
        if (mHashCode == 0) {
            HashCodeBuilder builder = new HashCodeBuilder(17, 31);
            builder.append(getComponent().hashCode());
            builder.append(getId().hashCode());
            // security labels not relevant since they MUST be the same by definition
            mHashCode = builder.toHashCode();
        }
        return mHashCode;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof InternalSource) {
            return super.equals(o);
        }
        return false;
    }
}
