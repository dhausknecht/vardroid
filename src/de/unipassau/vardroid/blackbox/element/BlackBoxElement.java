package de.unipassau.vardroid.blackbox.element;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import de.unipassau.vardroid.Config;
import de.unipassau.vardroid.blackbox.ComponentBlackBox;
import de.unipassau.vardroid.blackbox.SecurityLabel;

/**
 * TODO
 *
 * @author Daniel Hausknecht
 */
public abstract class BlackBoxElement {
    private final String mId;
    private final ComponentBlackBox mComponent;
    private Set<SecurityLabel> mSecurityLabels;
    private String mSecurityLabelString;
    
    protected int mHashCode = 0;
    private int mSecurityLabelHash = 0;
    
    public BlackBoxElement(String id, ComponentBlackBox cbb) {
        mId = id;
        mComponent = cbb;
        mSecurityLabels = new HashSet<SecurityLabel>();
    }
    
    /**
     * @return      the element identifier
     */
    public String getId() {
        return mId;
    }

    /**
     * @return the mComponent
     */
    public ComponentBlackBox getComponent() {
        return mComponent;
    }
    
    /**
     * The security classification of the data sink
     * @return  the set of security labels
     */
    public Set<SecurityLabel> getSecurityLabels() {
        return mSecurityLabels;
    }
    
    public String securityLabelToString() {
        if (mSecurityLabelString == null) {
            if (mSecurityLabels.isEmpty()) {
                mSecurityLabelString = "EMPTY";
            } else {
                mSecurityLabelString = "";
                boolean isFirst = true;
                for (SecurityLabel sl : mSecurityLabels) {
                    if (isFirst) {
                        mSecurityLabelString += sl.toString();
                        isFirst = false;
                    } else {
                        mSecurityLabelString += "_" + sl.toString();
                    }
                }
            }
        }
        return mSecurityLabelString;
    }
    
    public int getSecurityLabelHash() {
        if (mSecurityLabelHash == 0) {
            HashCodeBuilder builder = new HashCodeBuilder(17, 31);
            for (SecurityLabel label : this.getSecurityLabels()) {
                builder.append(label.hashCode());
            }
            mSecurityLabelHash = builder.toHashCode();
        }
        return mSecurityLabelHash;
    }
    
    /**
     * Update the current {@link SecurityLabel}s with the given one
     * @param secLabels     the {@link SecurityLabel}
     */
    public void updateSecurityLabels(SecurityLabel secLabel) {
        mSecurityLabels.add(secLabel);
        mSecurityLabelHash = 0;
        mSecurityLabelString = null;
        mHashCode = 0;
    }
    
    /**
     * Update the current {@link SecurityLabel}s with the given ones
     * @param secLabels     the {@link Set} of {@link SecurityLabel}s
     */
    public void updateSecurityLabels(Set<SecurityLabel> secLabels) {
        mSecurityLabels.addAll(secLabels);
        mSecurityLabelHash = 0;
        mSecurityLabelString = null;
        mHashCode = 0;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof BlackBoxElement) {
            BlackBoxElement elem = (BlackBoxElement) o;
            boolean isEqual = (this.hashCode() == elem.hashCode());
            // a cleaner implementation would do the following lines in the fitting PropagationHandler
            // but hey, it works this way. When you're bored and can't sleep, help yourself and change it!
            if (Config.getConfig().getMergeMode() == Config.MERGE_MODE_NONE) {
                isEqual = isEqual && (this.getComponent().getId() == elem.getComponent().getId());
            }
            if (Config.getConfig().getMergeMode() == Config.MERGE_MODE_ELEM_LEVEL) {
                isEqual = isEqual && (this.getComponent().getCallDepth() == elem.getComponent().getCallDepth());
            }
            return isEqual;
        }
        return false;
    }
    
    @Override
    public String toString() {
        return mComponent.getId() + "_" + mId + "_" + securityLabelToString();
    }
}
