package de.unipassau.vardroid.blackbox.element;

import de.unipassau.vardroid.blackbox.ComponentBlackBox;
import de.unipassau.vardroid.blackbox.SecurityLabel;

/**
 * TODO
 *
 * @author Daniel Hausknecht
 */
public abstract class DataSink extends BlackBoxElement {
    
    public DataSink(String id, ComponentBlackBox cbb) {
        super(id, cbb);
    }
    
    public DataSink(String id, ComponentBlackBox cbb, DataSink original) {
        super(id, cbb);
        for (SecurityLabel label : original.getSecurityLabels()) {
            updateSecurityLabels(new SecurityLabel(label));
        }
    }
}
