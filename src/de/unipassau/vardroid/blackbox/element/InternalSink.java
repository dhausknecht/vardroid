package de.unipassau.vardroid.blackbox.element;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import de.unipassau.vardroid.blackbox.ComponentBlackBox;

/**
 * Interface for a component-internal data sink as part of a component black-box.
 *
 * @author Daniel Hausknecht
 */
public class InternalSink extends DataSink {
    
    /**
     * Create a new {@link InternalSink} object
     * @param id        the {@link InternalSink} identifier
     * @param cbb
     */
    public InternalSink(String id, ComponentBlackBox cbb) {
        super(id, cbb);
    }
    
    /**
     * Copy constructor for a new {@link InternalSink} instance
     * @param id
     * @param cbb
     * @param original      the {@link InternalSink} to copy
     */
    public InternalSink(String id, ComponentBlackBox cbb, InternalSink original) {
        super(id, cbb, original);
    }
    
    @Override
    public int hashCode() {
        if (mHashCode == 0) {
            HashCodeBuilder builder = new HashCodeBuilder(17, 31).append(getComponent().hashCode()).append(getId().hashCode());
            builder.append(getSecurityLabelHash());
            mHashCode = builder.toHashCode();
        }
        return mHashCode;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof InternalSink) {
            return super.equals(o);
        }
        return false;
    }
}
