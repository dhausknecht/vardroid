package de.unipassau.vardroid.blackbox.element;

import java.util.Collection;
import java.util.LinkedList;

import javax.lang.model.type.UnknownTypeException;

import de.unipassau.vardroid.blackbox.ComponentBlackBox;
import de.unipassau.vardroid.blackbox.SecurityLabel;
import de.unipassau.vardroid.log.Log;

public abstract class DataSource extends BlackBoxElement {
    private Collection<DataSink> mSuccessors;

    public DataSource(String id, ComponentBlackBox cbb) {
        super(id, cbb);
        mSuccessors = new LinkedList<DataSink>();
    }
    
    public DataSource(String id, ComponentBlackBox cbb, DataSource original) {
        super(id, cbb);
        mSuccessors = new LinkedList<DataSink>();
        for (DataSink sink : original.getSinks()) {
            if (sink instanceof OutputInterface) {
                OutputInterface oldOutput = (OutputInterface) sink;
                OutputInterface output = cbb.getOutputInterface(oldOutput.getId());
                mSuccessors.add(output);
                
            } else if (sink instanceof InternalSink) {
                InternalSink oldSink = (InternalSink) sink;
                InternalSink intSink = cbb.getInternalSink(oldSink.getId());
                mSuccessors.add(intSink);
                
            } else {
                Log.e("Unknown data sink type while copying DataSource object.");
                throw new UnknownTypeException(null, sink);
            }
        }
        for (SecurityLabel label : original.getSecurityLabels()) {
            updateSecurityLabels(new SecurityLabel(label));
        }
    }

    /**
     * The set of directly from this data source reachable successors
     * @return      the set of successors
     */
    public Collection<DataSink> getSinks() {
        mHashCode = 0;
        return mSuccessors;
    }

    /**
     * @param sink      the {@link DataSink} to be added
     */
    public void addSuccessor(DataSink sink) {
        mSuccessors.add(sink);
        mHashCode = 0;
    }
    
    public void addSuccessors(Collection<DataSink> sinks) {
        mSuccessors.addAll(sinks);
        mHashCode = 0;
    }
    
    public boolean hasSuccessor(DataSink sink) {
        return mSuccessors.contains(sink);
    }
    
    public void removeSuccessor(DataSink sink) {
        // don't know why mSuccessors.removeAll doesn't do the job, but this here does it instead
        while (mSuccessors.contains(sink)) {
            mSuccessors.remove(sink);
        }
        mHashCode = 0;
    }
}
