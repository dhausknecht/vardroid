package de.unipassau.vardroid.blackbox;

import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * TODO
 *
 * @author Daniel Hausknecht
 */
public class SecurityLabel {
    public static enum CONFIDENTIALITY {PRIVATE, PUBLIC, L0, L1, L2, L3, L4, L5};
    
    private final CONFIDENTIALITY mConfidentiality;
    
    private int mHashCode = 0;
    
    /**
     * Construct a new {@link SecurityLabel} instance
     * @param confidentiality
     */
    public SecurityLabel(CONFIDENTIALITY confidentiality) {
        mConfidentiality = confidentiality;
    }
    
    /**
     * Copy constructor for a new {@link SecurityLabel} instance
     * @param original      the {@link SecurityLabel} to copy
     */
    public SecurityLabel(SecurityLabel original) {
        mConfidentiality = original.getConfidentiality();
    }
    
    public CONFIDENTIALITY getConfidentiality() {
        return mConfidentiality;
    }
    
    public int hashCode() {
        if (mHashCode == 0) {
            mHashCode = new HashCodeBuilder(17, 31).append(mConfidentiality.hashCode()).toHashCode();
        }
        return mHashCode;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SecurityLabel) {
            SecurityLabel sl = (SecurityLabel) obj;
            return this.mConfidentiality.equals(sl.getConfidentiality());
        }
        return false;
    }
    
    @Override
    public String toString() {
        return mConfidentiality.toString();
    }
}
