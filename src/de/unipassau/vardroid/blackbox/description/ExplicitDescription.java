package de.unipassau.vardroid.blackbox.description;

/**
 * A black-box interface description for explicit Intents.
 *
 * @author Daniel Hausknecht
 */
public class ExplicitDescription implements Description {
    private final String mComponentName;
    
    /**
     * Create a new {@link ExplicitDescription} object
     * @param componentName     the name of the target component
     */
    public ExplicitDescription(String componentName) {
        mComponentName = componentName;
    }
    
    /**
     * @return      the name of the target component
     */
    public String getComponentName() {
        return mComponentName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ExplicitDescription) {
            ExplicitDescription ed = (ExplicitDescription) obj;
            return this.getComponentName().equals(ed.getComponentName());
        }
        return false;
    }

    @Override
    public boolean isLaunchDescription() {
        return false;
    }
}
