package de.unipassau.vardroid.blackbox.description;

import java.util.HashSet;
import java.util.Set;

/**
 * A black-box interface description for intent-filters and implicit intents.
 *
 * @author Daniel Hausknecht
 */
public class ImplicitDescription implements Description {
    private Set<String> mActions;
    private Set<String> mCategories;
    
    /**
     * Create a new {@link ImplicitDescription} object
     * @param action        a intent-filter must have at least one action element
     */
    public ImplicitDescription(String action) {
        mActions = new HashSet<String>();
        mCategories = new HashSet<String>();
        mActions.add(action);
    }
    
    /**
     * @return      the defined actions
     */
    public Set<String> getActions() {
        return mActions;
    }
    
    /**
     * @return      the defined categories
     */
    public Set<String> getCategories() {
        return mCategories;
    }

    public void addAction(String action) {
        mActions.add(action);
    }

    public void addCategory(String category) {
        mCategories.add(category);
    }
    
    /**
     * Check whether the implicit component description matches the requirements of the intent according 
     * to the Android specifications. 
     * @param impDesc   the implicit description of the intent
     * @return          <code>true</code> if the description matches the intents requirements,
     *                  <code>false</code> otherwise
     */
    public boolean satisfiesIntent(ImplicitDescription impDesc) {
        return (this.getActions().containsAll(impDesc.getActions())                // this.actions >= impDesc.actions
                && this.getCategories().containsAll(impDesc.getCategories())    // this.categories == impDesc.categories
                && impDesc.getCategories().containsAll(this.getCategories()));
    }

    @Override
    public boolean isLaunchDescription() {
        return (this.getActions().contains("android.intent.action.MAIN")
                && this.getCategories().contains("android.intent.category.LAUNCHER"));
    }
}
