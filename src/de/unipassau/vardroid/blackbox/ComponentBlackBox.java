package de.unipassau.vardroid.blackbox;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import de.unipassau.vardroid.Config;
import de.unipassau.vardroid.blackbox.description.Description;
import de.unipassau.vardroid.blackbox.description.ImplicitDescription;
import de.unipassau.vardroid.blackbox.element.InputInterface;
import de.unipassau.vardroid.blackbox.element.InternalSink;
import de.unipassau.vardroid.blackbox.element.InternalSource;
import de.unipassau.vardroid.blackbox.element.OutputInterface;
import de.unipassau.vardroid.log.Log;

/**
 * The abstract representation of an Android component as a black-box
 *
 * @author Daniel Hausknecht
 */
public class ComponentBlackBox {
    private final String mId;
    private final String mType;
    private boolean isLaunchable;
    private InputInterface mStartupInterface;
    private final int mCallDepth;
    
    private int mHashCode = 0;
    
    private Map<String, InternalSource> mInternalSources;
    private Map<String, InternalSink> mInternalSinks;
    
    private Map<String, InputInterface> mInputInterfaces;
    private Map<String, OutputInterface> mOutputInterfaces;
    
    private Set<Permission> mGrantedPermissions;
    
    /**
     * Construct a {@link ComponentBlackBox} instance
     * @param id        the valid Android component identifier (e.g. pack.age.MyActivity)
     */
    public ComponentBlackBox(String id, int callDepth) {
        mId = id;
        mType = id;
        mCallDepth = callDepth;
        mInternalSources = new HashMap<String, InternalSource>();
        mInternalSinks = new HashMap<String, InternalSink>();
        mInputInterfaces = new HashMap<String, InputInterface>();
        mOutputInterfaces = new HashMap<String, OutputInterface>();
        mGrantedPermissions = new HashSet<Permission>();
    }
    
    /**
     * Copy constructor for a new {@link ComponentBlackBox} instance (deep copy)
     * @param original      the {@link ComponentBlackBox} to copy
     * @param instanceId    the identifier for the particular component instance
     */
    public ComponentBlackBox(ComponentBlackBox original, String instanceId, int callDepth) {
        mId = original.getId() + instanceId;
        mType = original.getType();
        isLaunchable = original.isLaunchable();
        mCallDepth = callDepth;
        
        // NOTE: do NOT change the order! DataSources need the sinks in their copy constructor
        // sinks
        mOutputInterfaces = new HashMap<String, OutputInterface>(original.getOutputInterfaces().size());
        for (OutputInterface output : original.getOutputInterfaces()) {
            mOutputInterfaces.put(output.getId(), new OutputInterface(output.getId(), this, output));
        }
        mInternalSinks = new HashMap<String, InternalSink>(original.getInternalSinks().size());
        for (InternalSink sink : original.getInternalSinks()) {
            mInternalSinks.put(sink.getId(), new InternalSink(sink.getId(), this, sink));
        }
        // sources
        mInputInterfaces = new HashMap<String, InputInterface>(original.getInputInterfaces().size());
        for (InputInterface input : original.getInputInterfaces()) {
            mInputInterfaces.put(input.getId(), new InputInterface(input.getId(), this, input));
        }
        mInternalSources = new HashMap<String, InternalSource>(original.getInternalSources().size());
        for (InternalSource source : original.getInternalSources()) {
            mInternalSources.put(source.getId(), new InternalSource(source.getId(), this, source));
        }
        
        InputInterface originalInput = original.getStartupInterface();
        mStartupInterface = new InputInterface(originalInput.getId(), this, originalInput);
        
        mGrantedPermissions = original.getGrantedPermissions();
    }
    
    @Override
    public String toString() {
        String result = "Id: " + getId() + '\n';
        result += "\tlaunchable: " + isLaunchable() + '\n';
        result += "\tinternal sources:\n";
        for (InternalSource source : getInternalSources()) {
            result += "\t\t" + source.getId() + '\n';
        }
        result += "\tinternal sinks:\n";
        for (InternalSink sink : getInternalSinks()) {
            result += "\t\t" + sink.getId() + '\n';
        }
        result += "\tinput-interfaces:\n";
        for (InputInterface input : getInputInterfaces()) {
            result += "\t\t" + input.getId() + '\n';
        }
        result += "\toutput-interfaces:\n";
        for (OutputInterface output : getOutputInterfaces()) {
            result += "\t\t" + output.getId() + '\n';
        }
        return result;
    }
    
    @Override
    public int hashCode() {
        if (mHashCode == 0) {
            HashCodeBuilder builder = new HashCodeBuilder(17, 31);
            if (Config.getConfig().getMergeMode() == Config.MERGE_MODE_NONE) {
                builder.append(getId());
            } else {
                builder.append(getType());
            }
            int elements = 0;
            for (InternalSource is : mInternalSources.values()) {
//                builder.append(is.getId().hashCode());
                elements += is.getId().hashCode();
            }
            for (InternalSink sink : mInternalSinks.values()) {
//                builder.append(sink.getId().hashCode());
                elements += sink.getId().hashCode();
            }
            for (InputInterface in : mInputInterfaces.values()) {
//                builder.append(in.getId().hashCode());
                elements += in.getId().hashCode();
            }
            for (OutputInterface out : mOutputInterfaces.values()) {
//                builder.append(out.getId().hashCode());
                elements += out.getId().hashCode();
            }
            builder.append(elements);
            mHashCode = builder.toHashCode();
        }
        return mHashCode;
    }
    
    /**
     * @return      the valid Android component identifier
     */
    public String getId() {
        return mId;
    }
    
    public String getType() {
        return mType;
    }
    
    public boolean isLaunchable() {
        return isLaunchable;
    }
    
    public int getCallDepth() {
        return mCallDepth;
    }
    
    /**
     * @return      the {@link InputInterface} that is accessed on component launching
     */
    public InputInterface getLaunchableInputInterface() {
        if (!isLaunchable) {
            Log.e("Component " + this.getId() + " is not launchable!");
            throw new IllegalStateException("Component " + this.getId() + "is not launchable!");
        }
        return mStartupInterface;
    }
    
    public InputInterface getStartupInterface() {
        if (mStartupInterface == null) {
            Log.e("No startup input interface defined for component " + this.getId());
            throw new NullPointerException();
        }
        return mStartupInterface;
    }
    
    public void setStartupInterface(String inputId) {
        if (mStartupInterface == null) {
            this.mStartupInterface = getInputInterface(inputId);
        } else {
            Log.e("Startup input interface already defined for " + this.getId());
            throw new IllegalStateException();
        }
        mHashCode = 0;
    }
    
    /**
     * @return the mInternalSources
     */
    public Collection<InternalSource> getInternalSources() {
        return mInternalSources.values();
    }
    
    /**
     * @param id    the identifier for the wanted {@link InternalSource}
     * @return      the {@link InternalSource} with the given identifier
     */
    public InternalSource getInternalSource(String id) {
        InternalSource source = this.mInternalSources.get(id);
        if (source == null) {
            Log.e("Internal source \"" + id + "\" does not exist!");
            throw new NullPointerException();
        }
        return source;
    }
    
    /**
     * @return the mInternalSinks
     */
    public Collection<InternalSink> getInternalSinks() {
        return mInternalSinks.values();
    }
    
    /**
     * @param id    the identifier for the wanted {@link InternalSink}
     * @return      the {@link InternalSink} with the given identifier
     */
    public InternalSink getInternalSink(String id) {
        InternalSink sink = this.mInternalSinks.get(id);
        if (sink == null) {
            Log.e("Internal sink \"" + id + "\" does not exist!");
            throw new NullPointerException();
        }
        return sink;
    }
    
    /**
     * @return the mInputInterfaces
     */
    public Collection<InputInterface> getInputInterfaces() {
        return mInputInterfaces.values();
    }
    
    /**
     * @param id    the identifier for the wanted {@link InputInterface}
     * @return      the {@link InputInterface} with the given identifier
     */
    public InputInterface getInputInterface(String id) {
        InputInterface input = this.mInputInterfaces.get(id);
        if (input == null) {
            Log.e("Input-interface \"" + id + "\" does not exist!");
            throw new NullPointerException();
        }
        return input;
    }
    
    /**
     * @return the mOutputInterfaces
     */
    public Collection<OutputInterface> getOutputInterfaces() {
        return mOutputInterfaces.values();
    }
    
    /**
     * @param id    the identifier for the wanted {@link OutputInterface}
     * @return      the {@link OutputInterface} with the given identifier
     */
    public OutputInterface getOutputInterface(String id) {
        OutputInterface output = this.mOutputInterfaces.get(id);
        if (output == null) {
            Log.e("Output-interface \"" + id + "\" does not exist!");
            throw new NullPointerException();
        }
        return output;
    }
    
    /**
     * Add a new {@link InternalSource} to the {@link ComponentBlackBox}
     * @param sourceId       the {@link InternalSource} identifier
     */
    public void addInternalSource(String sourceId) {
        mInternalSources.put(sourceId, new InternalSource(sourceId, this));
        mHashCode = 0;
    }
    
    /**
     * Add a new {@link InternalSink} to the {@link ComponentBlackBox}
     * @param inputId       the {@link InternalSink} identifier
     */
    public void addInternalSink(String sinkId) {
        mInternalSinks.put(sinkId, new InternalSink(sinkId, this));
        mHashCode = 0;
    }
    
    /**
     * Add a new {@link InputInterface} to the {@link ComponentBlackBox}
     * @param inputId       the {@link InputInterface} identifier
     */
    public void addInputInterface(String inputId) {
        mInputInterfaces.put(inputId, new InputInterface(inputId, this));
        mHashCode = 0;
    }
    
    /**
     * Add a new {@link OutputInterface} to the {@link ComponentBlackBox}
     * @param outputId       the {@link OutputInterface} identifier
     */
    public void addOutputInterface(String outputId) {
        mOutputInterfaces.put(outputId, new OutputInterface(outputId, this));
    }
    
    /**
     * Create a transition from an {@link InputInterface} to an {@link InternalSink}
     * @param input     the {@link InputInterface}
     * @param sink      the {@link InternalSink}
     */
    public void connect(InputInterface input, InternalSink sink) {
        input.addSuccessor(sink);
        mHashCode = 0;
    }
    
    /**
     * Create a transition from an {@link InputInterface} to an {@link OutputInterface}
     * @param input     the {@link InputInterface}
     * @param output    the {@link OutputInterface}
     */
    public void connect(InputInterface input, OutputInterface output) {
        input.addSuccessor(output);
        mHashCode = 0;
    }
    
    /**
     * Create a transition from an {@link InternalSource} to an {@link InternalSink}
     * @param source    the {@link InternalSource}
     * @param sink      the {@link InternalSink}
     */
    public void connect(InternalSource source, InternalSink sink) {
        source.addSuccessor(sink);
        mHashCode = 0;
    }
    
    /**
     * Create a transition from an {@link InternalSource} to an {@link OutputInterface}
     * @param source    the {@link InternalSource}
     * @param output    the {@link OutputInterface}
     */
    public void connect(InternalSource source, OutputInterface output) {
        source.addSuccessor(output);
        mHashCode = 0;
    }

    /**
     * Create a transition from an {@link InputInterface} to an {@link InternalSink}
     * @param inputId       the {@link InputInterface} identifier
     * @param sinkId        the {@link InternalSink} identifier
     */
    public void connectInputInterfaceInternalSink(String inputId, String sinkId) {
        InputInterface input = this.getInputInterface(inputId);
        InternalSink sink = this.getInternalSink(sinkId);
        input.addSuccessor(sink);
        mHashCode = 0;
    }

    /**
     * Create a transition from an {@link InputInterface} to an {@link OutputInterface}
     * @param inputId       the {@link InputInterface} identifier
     * @param outputId      the {@link OutputInterface} identifier
     */
    public void connectInputInterfaceOutputInterface(String inputId, String outputId) {
        InputInterface input = this.getInputInterface(inputId);
        OutputInterface output = this.getOutputInterface(outputId);
        input.addSuccessor(output);
        mHashCode = 0;
    }

    /**
     * Create a transition from an {@link InternalSource} to an {@link InternalSink}
     * @param sourceId      the {@link InternalSource} identifier
     * @param sinkId        the {@link InternalSink} identifier
     */
    public void connectInternalSourceInternalSink(String sourceId, String sinkId) {
        InternalSource source = this.getInternalSource(sourceId);
        InternalSink sink = this.getInternalSink(sinkId);
        source.addSuccessor(sink);
        mHashCode = 0;
    }

    /**
     * Create a transition from an {@link InternalSource} to an {@link OutputInterface}
     * @param sourceId      the {@link InternalSource} identifier
     * @param outputId      the {@link OutputInterface} identifier
     */
    public void connectInternalSourceOutputInterface(String sourceId, String outputId) {
        InternalSource source = this.getInternalSource(sourceId);
        OutputInterface output = this.getOutputInterface(outputId);
        source.addSuccessor(output);
        mHashCode = 0;
    }
    
    /**
     * Set the {@link Description} of an {@link InputInterface}, i.e. its Intnet-Filter
     * @param inputId           the {@link InputInterface} identifier
     * @param description       the {@link Description}
     */
    public void addInputInterfaceDescription(String inputId, ImplicitDescription description) {
        InputInterface input = this.getInputInterface(inputId);
        input.addDescription(description);
        if (description.isLaunchDescription()) {
            if (!this.isLaunchable()) {
                this.isLaunchable = true;
                this.mStartupInterface = input;
            } else {
                Log.e("Launchable interface for component " + this.getId() + " already exists");
                throw new IllegalStateException();
            }
        }
        mHashCode = 0;
    }
    
    /**
     * Add a {@link Description} of an {@link OutputInterface}, i.e. a possible Intent configuration
     * @param outputId          the {@link OutputInterface} identifier
     * @param description       the {@link Description}
     */
    public void setOutputInterfaceDescription(String outputId, Description description) {
        OutputInterface output = this.getOutputInterface(outputId);
        output.setDescription(description);
        mHashCode = 0;
    }
    
    /**
     * TODO
     * @param inputId
     * @param conf
     */
    public void addInputInterfaceSecurityLabel(String inputId, SecurityLabel.CONFIDENTIALITY conf) {
        InputInterface input = getInputInterface(inputId);
        input.updateSecurityLabels(new SecurityLabel(conf));
        mHashCode = 0;
    }
    
    /**
     * TODO
     * @param outputId
     * @param conf
     */
    public void addOutputInterfaceSecurityLabel(String outputId, SecurityLabel.CONFIDENTIALITY conf) {
        OutputInterface output = getOutputInterface(outputId);
        output.updateSecurityLabels(new SecurityLabel(conf));
        mHashCode = 0;
    }
    
    /**
     * TODO
     * @param sourceId
     * @param conf
     */
    public void addInternalSourceSecurityLabel(String sourceId, SecurityLabel.CONFIDENTIALITY conf) {
        InternalSource source = getInternalSource(sourceId);
        source.updateSecurityLabels(new SecurityLabel(conf));
        mHashCode = 0;
    }
    
    /**
     * TODO
     * @param sinkId
     * @param conf
     */
    public void addInternalSinkSecurityLabel(String sinkId, SecurityLabel.CONFIDENTIALITY conf) {
        InternalSink sink = getInternalSink(sinkId);
        sink.updateSecurityLabels(new SecurityLabel(conf));
        mHashCode = 0;
    }
    
    /**
     * Add a {@link Permission} granted to the represented component.
     * @param p     the granted {@link Permission}
     */
    public void addGrantedPermission(Permission p) {
        mGrantedPermissions.add(p);
    }
    
    private Set<Permission> getGrantedPermissions() {
        return mGrantedPermissions;
    }

    public boolean isPermissionGranted(InputInterface input) {
        return mGrantedPermissions.containsAll(input.getPermissions());
    }
    
}
