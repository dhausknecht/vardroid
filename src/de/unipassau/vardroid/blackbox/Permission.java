package de.unipassau.vardroid.blackbox;

/**
 * TODO
 *
 * @author Daniel Hausknecht
 */
public class Permission {
    private final String mId;
    
    /**
     * Create a {@link Permission} object.
     * @param id    the Android permission identifier
     */
    public Permission(String id) {
        mId = id;
    }
    
    /**
     * @return      the Android permission identifier
     */
    public String getId() {
        return mId;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Permission) {
            return mId.equals(((Permission) obj).getId());
        }
        return false;
    }

}
