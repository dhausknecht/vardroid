package de.unipassau.vardroid.conflict;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import de.unipassau.vardroid.blackbox.SecurityLabel;

/**
 * Class representing a {@link SecurityLabel} conflict situation.
 *
 * @author Daniel Hausknecht
 */
public class Conflict {
    private final int mId;
    private final Set<SecurityLabel> mConflictSet;
    private int mHashCode;
    
    public Conflict(int id) {
        mId = id;
        mConflictSet = new HashSet<SecurityLabel>();
    }
    
    public int getId() {
        return mId;
    }
    
    public void addConflictingSecurityLabel(SecurityLabel label) {
        mConflictSet.add(label);
        mHashCode = 0;
    }
    
    /**
     * Check whether conflict represented by {@link Conflict} instance arises. 
     * @param labels        the {@link SecurityLabel} to check
     * @return              <code>true</code> on conflict, <code>false</code> otherwise
     */
    public boolean occurs(Set<SecurityLabel> labels) {
        return labels.containsAll(mConflictSet);
    }
    
    @Override
    public int hashCode() {
        if (mHashCode == 0) {
            HashCodeBuilder builder = new HashCodeBuilder(17, 31).append(mId);
            for (SecurityLabel label : mConflictSet) {
                builder.append(label.hashCode());
            }
            mHashCode = builder.toHashCode();
        }
        return mHashCode;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof Conflict) {
            Conflict c = (Conflict) o;
            return (this.hashCode() == c.hashCode());
        }
        return false;
    }
    
    @Override
    public String toString() {
        String result = null;
        for (SecurityLabel label : mConflictSet) {
            if (result == null) {
                result = label.toString();
            } else {
                result += "_" + label.toString();
            }
        }
        return result;
    }
}
