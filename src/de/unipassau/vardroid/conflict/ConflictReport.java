package de.unipassau.vardroid.conflict;

import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Object holding all relevant information related to a conflict occurrence.
 *
 * @author Daniel Hausknecht
 */
public final class ConflictReport<NODE> {
    private final Conflict mConflict;
    private final NODE mNode;
    private int mHashCode = 0;
    
    public ConflictReport(Conflict conflict, NODE node) {
        mConflict = conflict;
        mNode = node;
    }
    
    public Conflict getConflict() {
        return mConflict;
    }
    
    public NODE getElement() {
        return mNode;
    }
    
    @Override
    public int hashCode() {
        if (mHashCode == 0) {
            HashCodeBuilder builder = new HashCodeBuilder(17, 31);
            builder.append(mConflict.hashCode()).append(mNode.hashCode());
            mHashCode = builder.toHashCode();
        }
        return mHashCode;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof ConflictReport) {
            ConflictReport<?> cr = (ConflictReport<?>) o;
            return (this.hashCode() == cr.hashCode());
        }
        return false;
    }
}
