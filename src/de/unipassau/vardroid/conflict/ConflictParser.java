package de.unipassau.vardroid.conflict;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import de.unipassau.vardroid.blackbox.SecurityLabel;
import de.unipassau.vardroid.log.Log;

public class ConflictParser {
    /**
     * Parse a set of conflict definitions from XML file.
     * @param file      the file path
     * @return
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    public static <NODE> ConflictDetector<NODE> parseXML(String file, ConflictDetector<NODE> detector) {
        try {
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            parser.parse(new File(file), new ConflictParserHandler<NODE>(detector));
        } catch (SAXException e) {
            Log.e(e.getMessage());
        } catch (IOException e) {
            Log.e(e.getMessage());
        } catch (ParserConfigurationException e) {
            Log.e(e.getMessage());
        }
        
        return detector;
    }
    
    /**
     * Extension of {@link DefaultHandler} for parsing XML files encoding a set of conflicts
     *
     * @author Daniel Hausknecht
     */
    private static class ConflictParserHandler<NODE> extends DefaultHandler {
        private final ConflictDetector<NODE> mConflictDetector;
        
        private int mConflictCounter;
        private Conflict mCurrentConflict;
        
        private Map<String, SecurityLabel> mSecurityLabelMap;
        
        public ConflictParserHandler(ConflictDetector<NODE> detector) {
            mConflictDetector = detector;
            mConflictCounter = 0;
            mSecurityLabelMap = new HashMap<String, SecurityLabel>();
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            // conflict
            if (qName.equals("conflict")) {
                mCurrentConflict = new Conflict(mConflictCounter);
                mConflictCounter++;
                
            // label
            } else if (qName.equals("label")) {
                String id = attributes.getValue("conf");
                SecurityLabel secLabel = mSecurityLabelMap.get(id);
                if (secLabel == null) {
                    String conf = id.toUpperCase();
                    for (SecurityLabel.CONFIDENTIALITY label : SecurityLabel.CONFIDENTIALITY.values()) {
                        if (label.toString().equals(conf)) {
                            secLabel = new SecurityLabel(label);
                            mSecurityLabelMap.put(id, secLabel);
                        }
                    }
                }
                mCurrentConflict.addConflictingSecurityLabel(secLabel);
            }
        }
        
        @Override
        public void endElement(String uri, String localName, String qName) {
            // conflict
            if (qName.equals("conflict")) {
                mConflictDetector.addConflict(mCurrentConflict);
                mCurrentConflict = null;
            }
        }
    }
}
