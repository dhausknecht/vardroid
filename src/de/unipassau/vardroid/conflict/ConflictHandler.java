package de.unipassau.vardroid.conflict;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation for reacting on security conflict detection.
 *
 * @author Daniel Hausknecht
 */
public class ConflictHandler<NODE> {
    private final Set<ConflictReport<NODE>> mConflictReports;
    
    public ConflictHandler() {
        mConflictReports = new HashSet<ConflictReport<NODE>>();
    }
    
    public void addConflictReports(Collection<ConflictReport<NODE>> conflictReports) {
        mConflictReports.addAll(conflictReports);
    }

    /**
     * @return      the number of detected conflicts
     */
    public int detected() {
        return mConflictReports.size();
    }
}
