package de.unipassau.vardroid.conflict;

import java.util.HashSet;
import java.util.Set;

import de.unipassau.vardroid.blackbox.element.BlackBoxElement;

/**
 * The {@link ConflictDetector} holds the security conflict definitions and checks for their 
 * occurrences.
 *
 * @author Daniel Hausknecht
 */
public abstract class ConflictDetector<NODE> {
    private final Set<Conflict> mConflicts;
    private final ConflictHandler<NODE> mConflictHandler;
    
    protected ConflictDetector(ConflictHandler<NODE> conflictHandler) {
        mConflicts = new HashSet<Conflict>();
        mConflictHandler = conflictHandler; 
    }
    
    public ConflictHandler<NODE> getConflictHandler() {
        return mConflictHandler;
    }
    
    protected Set<Conflict> getConflicts() {
        return mConflicts;
    }
    
    /**
     * Add a security conflict definition
     * @param conflict      the security conflict
     */
    void addConflict(Conflict conflict) {
        mConflicts.add(conflict);
    }

    /**
     * Check whether a security label conflict exists at the {@link BlackBoxElement}. 
     * @param element       the {@link BlackBoxElement} which security label will be checked
     */
    public abstract void checkForConflict(NODE node);

    /**
     * @return      the number of defined security conflicts
     */
    public int size() {
        return mConflicts.size();
    }
    
    /**
     * @return      the number of detected conflicts
     */
    public int detected() {
        return mConflictHandler.detected();
    }
}
