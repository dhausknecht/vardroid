package de.unipassau.vardroid.conflict;

import java.util.Collection;
import java.util.LinkedList;

import de.unipassau.vardroid.blackbox.element.BlackBoxElement;
import de.unipassau.vardroid.state.State;

public final class StateConflictDetector extends ConflictDetector<State<? extends BlackBoxElement>> {

    public StateConflictDetector(ConflictHandler<State<? extends BlackBoxElement>> conflictHandler) {
        super(conflictHandler);
    }

    /**
     * Check whether a security label conflict exists at the {@link BlackBoxElement}. 
     * @param element       the {@link BlackBoxElement} which security label will be checked
     */
    public void checkForConflict(State<? extends BlackBoxElement> node) {
        Collection<ConflictReport<State<? extends BlackBoxElement>>> conflictReports =
                new LinkedList<ConflictReport<State<? extends BlackBoxElement>>>();
        for (Conflict conflict : getConflicts()) {
            if (conflict.occurs(node.getSecurityLabels())) {
                conflictReports.add(new ConflictReport<State<? extends BlackBoxElement>>(conflict, node));
            }
        }
        getConflictHandler().addConflictReports(conflictReports);
    }

}
