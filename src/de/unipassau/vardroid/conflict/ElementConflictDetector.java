package de.unipassau.vardroid.conflict;

import java.util.Collection;
import java.util.LinkedList;

import de.unipassau.vardroid.blackbox.element.BlackBoxElement;

public final class ElementConflictDetector extends ConflictDetector<BlackBoxElement> {

    public ElementConflictDetector(ConflictHandler<BlackBoxElement> conflictHandler) {
        super(conflictHandler);
    }

    /**
     * Check whether a security label conflict exists at the {@link BlackBoxElement}. 
     * @param element       the {@link BlackBoxElement} which security label will be checked
     */
    public void checkForConflict(BlackBoxElement node) {
        Collection<ConflictReport<BlackBoxElement>> conflictReports =
                new LinkedList<ConflictReport<BlackBoxElement>>();
        for (Conflict conflict : getConflicts()) {
            if (conflict.occurs(node.getSecurityLabels())) {
                conflictReports.add(new ConflictReport<BlackBoxElement>(conflict, node));
            }
        }
        getConflictHandler().addConflictReports(conflictReports);
    }
}
