package de.unipassau.vardroid.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import de.unipassau.vardroid.BlackBoxManager;
import de.unipassau.vardroid.blackbox.ComponentBlackBox;
import de.unipassau.vardroid.blackbox.Permission;
import de.unipassau.vardroid.blackbox.SecurityLabel;
import de.unipassau.vardroid.blackbox.description.ExplicitDescription;
import de.unipassau.vardroid.blackbox.description.ImplicitDescription;
import de.unipassau.vardroid.log.Log;

/**
 * Parser implementation for set of black-box components from a XML file. 
 *
 * @author Daniel Hausknecht
 */
public class ComponentXMLParser {
    private static final int NO_PARENT = 0;
    private static final int REQUIRED_PERMISSIONS = 1;
    private static final int GRANTED_PERMISSIONS = 2;
    
    private static int sParent = 0;
    
    /**
     * Parse a set of components from XML file.
     * @param file      the file path
     * @return
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    public static BlackBoxManager parseXML(String file) {
        BlackBoxManager result = new BlackBoxManager();
        
        try {
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            parser.parse(new File(file), new ComponentParserHandler(result));
        } catch (SAXException e) {
            Log.e(e.getMessage());
        } catch (IOException e) {
            Log.e(e.getMessage());
        } catch (ParserConfigurationException e) {
            Log.e(e.getMessage());
        }
        
        return result;
    }
    
    /**
     * Extension of {@link DefaultHandler} for parsing XML files encoding a set of black-box components
     *
     * @author Daniel Hausknecht
     */
    private static class ComponentParserHandler extends DefaultHandler {
        private static final int INPUT = 0;
        private static final int OUTPUT = 1;
        private static final int SOURCE = 2;
        private static final int SINK = 3;
        
        private final BlackBoxManager mBlackBoxManager;
        private ComponentBlackBox mCurrentComponent;
        private ImplicitDescription mCurrentImplicitDescription;
        private String mCurrentElementName;
        private int mCurrentElementType;
        
        private Map<String, Permission> mPermissionMap;
        
        public ComponentParserHandler(BlackBoxManager bbm) {
            mBlackBoxManager = bbm;
            mPermissionMap = new HashMap<String, Permission>();
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            
            // component
            if (qName.equals("component")) {
                String id = attributes.getValue("name");
                mCurrentComponent = new ComponentBlackBox(id, 0);
                
            // input
            } else if (qName.equals("input")) {
                String id = attributes.getValue("name");
                mCurrentComponent.addInputInterface(id);
                if ((attributes.getValue("startup") != null)
                        && attributes.getValue("startup").equals("true")) {
                    mCurrentComponent.setStartupInterface(id);
                }
                mCurrentElementType = INPUT;
                mCurrentElementName = id;
                
            // source
            } else if (qName.equals("source")) {
                String id = attributes.getValue("name");
                mCurrentComponent.addInternalSource(id);
                mCurrentElementType = SOURCE;
                mCurrentElementName = id;
                
            // sink
            } else if (qName.equals("sink")) {
                String id = attributes.getValue("name");
                mCurrentComponent.addInternalSink(id);
                mCurrentElementType = SINK;
                mCurrentElementName = id;
                
            // output
            } else if (qName.equals("output")) {
                String id = attributes.getValue("name");
                mCurrentComponent.addOutputInterface(id);
                mCurrentElementType = OUTPUT;
                mCurrentElementName = id;
                
            // action
            } else if (qName.equals("action")) {
                String action = attributes.getValue("name");
                if (mCurrentImplicitDescription == null) {
                    mCurrentImplicitDescription = new ImplicitDescription(action);
                } else {
                    mCurrentImplicitDescription.addAction(action);
                }
                
            // category
            } else if (qName.equals("category")) {
                String category = attributes.getValue("name");
                mCurrentImplicitDescription.addCategory(category);
                
            // explicit
            } else if (qName.equals("explicit")) {
                String target = attributes.getValue("target");
                switch (mCurrentElementType) {
                case OUTPUT:
                    mCurrentComponent.setOutputInterfaceDescription(mCurrentElementName, new ExplicitDescription(target));
                    break;
                default:
                    Log.e("Why an explicit description for something else than an output, stupid?");
                    System.exit(0);
                    break;
                }
                
            // label
            } else if (qName.equals("label")) {
                String conf = attributes.getValue("conf").toUpperCase();
                for (SecurityLabel.CONFIDENTIALITY label : SecurityLabel.CONFIDENTIALITY.values()) {
                    if (label.toString().equals(conf)) {
                        switch (mCurrentElementType) {
                        case INPUT:
                            mCurrentComponent.addInputInterfaceSecurityLabel(mCurrentElementName, label);
                            break;
                        case OUTPUT:
                            mCurrentComponent.addOutputInterfaceSecurityLabel(mCurrentElementName, label);
                            break;
                        case SOURCE:
                            mCurrentComponent.addInternalSourceSecurityLabel(mCurrentElementName, label);
                            break;
                        case SINK:
                            mCurrentComponent.addInternalSinkSecurityLabel(mCurrentElementName, label);
                            break;
                        default:
                            Log.e("Undefined something to add a security label to... :-(");
                            System.exit(0);
                            break;
                        }
                        break;
                    }
                }
                
            // relation
            } else if (qName.equals("relation")) {
                String origin = attributes.getQName(0);
                String value0 = attributes.getValue(0);
                String target = attributes.getQName(1);
                String value1 = attributes.getValue(1);
                if (origin.equals("input") && target.equals("output")) {
                    mCurrentComponent.connectInputInterfaceOutputInterface(value0, value1);
                } else if (origin.equals("input") && target.equals("sink")) {
                    mCurrentComponent.connectInputInterfaceInternalSink(value0, value1);
                } else if (origin.equals("source") && target.equals("sink")) {
                    mCurrentComponent.connectInternalSourceInternalSink(value0, value1);
                } else if (origin.equals("source") && target.equals("output")) {
                    mCurrentComponent.connectInternalSourceOutputInterface(value0, value1);
                } else {
                    Log.e("WTF is wrong with you? You can't just connect arbitrary stuff...");
                    System.exit(0);
                }
                
            // required-permissions
            } else if (qName.equals("required-permissions")) {
                sParent = REQUIRED_PERMISSIONS;
                
            // granted-permissions
            } else if (qName.equals("granted-permissions")) {
                sParent = GRANTED_PERMISSIONS;
                
            // permission
            } else if (qName.equals("permission")) {
                String id = attributes.getValue("name");
                Permission p = mPermissionMap.get(id);
                if (p == null) {
                    p = new Permission(id);
                    mPermissionMap.put(id, p);
                }
                switch (sParent) {
                case REQUIRED_PERMISSIONS:
                    mCurrentComponent.getInputInterface(mCurrentElementName).addRequiredPermission(p);
                    break;
                case GRANTED_PERMISSIONS:
                    mCurrentComponent.addGrantedPermission(p);
                    break;
                default:
                    Log.e("Permission defined in invalid parent element");
                    System.exit(0);
                }
            }
        }
        
        @Override
        public void endElement(String uri, String localName, String qName) {
            // input, output, sink, source
            if (qName.equals("component")) {
                mBlackBoxManager.addComponent(mCurrentComponent);
                
            // implicit
            } else if (qName.equals("implicit")) {
                switch (mCurrentElementType) {
                case INPUT:
                    mCurrentComponent.addInputInterfaceDescription(mCurrentElementName, mCurrentImplicitDescription);
                    break;
                case OUTPUT:
                    mCurrentComponent.setOutputInterfaceDescription(mCurrentElementName, mCurrentImplicitDescription);
                    break;
                default:
                    Log.e("Only input and outputs can have implicit descriptions!");
                    System.exit(0);
                    break;
                }
                mCurrentImplicitDescription = null;
                
            // required-permission
            } if (qName.equals("required-permissions")) {
                sParent = NO_PARENT;
                
            // granted-permission
            } if (qName.equals("granted-permissions")) {
                sParent = NO_PARENT;
            }
        }
    }
}
