/**
 * 
 */
package de.unipassau.vardroid.propagation;

import java.util.Collection;
import java.util.Set;

import de.unipassau.vardroid.BlackBoxManager;
import de.unipassau.vardroid.Config;
import de.unipassau.vardroid.blackbox.SecurityLabel;
import de.unipassau.vardroid.blackbox.element.BlackBoxElement;
import de.unipassau.vardroid.blackbox.element.InputInterface;
import de.unipassau.vardroid.blackbox.element.OutputInterface;
import de.unipassau.vardroid.conflict.ConflictDetector;
import de.unipassau.vardroid.connector.ComponentConnector;
import de.unipassau.vardroid.graph.Graph;
import de.unipassau.vardroid.log.Log;

/**
 * The {@link PropagationHandler} provides an abstract class for performing Phase 2 and Phase 3 
 * in two modes: either to create the whole call-graph before propagating security labels, 
 * or to find the successive components on demand while propagating.
 * A {@link ConflictDetector} can be set for detecting and handling possible security label conflicts.
 *
 * @author Daniel Hausknecht
 */
public abstract class PropagationHandler<IN extends NODE, OUT extends NODE, NODE> {
    private final ComponentConnector<IN, OUT> mComponentConnector;
    private ConflictDetector<NODE> mConflictDetector;
    
    private final Graph<NODE> mGraph;
    
    /**
     * Create a {@link PropagationHandler} instance.
     * @param compConn      the {@link ComponentConnector} handling the functionality of Phase 2
     */
    public PropagationHandler(ComponentConnector<IN, OUT> compConn, Graph<NODE> graph) {
        mComponentConnector = compConn;
        mGraph = graph;
    }
    
    public Graph<NODE> getGraph() {
        return mGraph;
    }
    
    public void setConflictDetector(ConflictDetector<NODE> conflictDetector) {
        mConflictDetector = conflictDetector;
    }
    
    /**
     * Find the component successors and propagate the security labels through the resulting graph.
     * @param bbm               the {@link BlackBoxManager} holding the available components
     * @param searchMode        either <code>Config.SUCC_MODE_SEPARATE</code> for first creating the call-graph and then propagating,
     *                          or <code>Config.SUCC_MODE_COMBINED</code> for finding the component successors on demand
     * @throws IllegalArgumentException     thrown on unknown search mode
     */
    public void propagateSecurityLabels(BlackBoxManager bbm, int searchMode) throws IllegalArgumentException {
        Collection<IN> startInputInterfaces = null;
        
        Log.d("Number of component black boxes: " + bbm.getNumberOfComponents());
        
        switch (Config.getConfig().getSuccessorSearchMode()) {
        case Config.SUCC_MODE_SEPARATE:
            // Phase 2
            startInputInterfaces = mComponentConnector.createComponentCallGraph(bbm);
            // Phase 3
            getGraph().insertAll(startInputInterfaces);
            propagateSecurityLabels(startInputInterfaces);
            break;
            
        case Config.SUCC_MODE_COMBINED:
            // get starting nodes
            startInputInterfaces = mComponentConnector.getLaunchableInputInterfaces(bbm);
            // propagate with finding successors on demand
            getGraph().insertAll(startInputInterfaces);
            propagateSecurityLabels(startInputInterfaces, bbm);
            break;
            
        default:
            throw new IllegalArgumentException("Unknown component successor search mode!");
        }
        
        Log.d("------- Statistics -------");
        Log.d("Graph size: " + getGraph().size());
        Log.d("Total element count: " + getGraph().totalNodeElementCount());
        Log.d("Min. node size: " + getGraph().minNodeSize());
        Log.d("Max. node size: " + getGraph().maxNodeSize());
        Log.d("Mean node size: " + getGraph().meanNodeSize() + '\n');
        Log.d("Defined conflicts: " + this.mConflictDetector.size());
        Log.d("Detected conflicts: " + this.mConflictDetector.detected());
        Log.d("--------------------------");
        getGraph().draw();
    }
    
    /**
     * The basic propagation algorithm of Phase 3.
     * @param startInputInterfaces      the {@link Collection} of all launchable input-interfaces
     */
    private void propagateSecurityLabels(Collection<IN> startInputInterfaces) {
        Config.getConfig().resetCurrentCallDepth();
        
        Collection<OUT> outputs = intraComponentAnalysis(startInputInterfaces);
        
        while (!outputs.isEmpty()
                && (Config.getConfig().getCurrentCallDepth() < Config.getConfig().getMaxAnalysisDepth())) {
            Config.getConfig().increaseCurrentCallDepth();
            Log.d("current depth: " + Config.getConfig().getCurrentCallDepth());
            Collection<IN> inputs = interComponentAnalysis(outputs);
            outputs = null;
            outputs = intraComponentAnalysis(inputs);
            inputs = null;
        }
    }
    
    /**
     * The propagation algorithm for combined Phase 2 and Phase 3.
     * @param startInputInterfaces      the {@link Collection} of all launchable input-interfaces
     * @param bbm                       the {@link BlackBoxManager} holding the available components
     */
    private void propagateSecurityLabels(Collection<IN> startInputInterfaces, BlackBoxManager bbm) {
        Config.getConfig().resetCurrentCallDepth();
        
        Collection<OUT> outputs = intraComponentAnalysis(startInputInterfaces);
        
        while (!outputs.isEmpty()
                && (Config.getConfig().getCurrentCallDepth() < Config.getConfig().getMaxAnalysisDepth())) {
            Config.getConfig().increaseCurrentCallDepth();
            Log.d("current depth: " + Config.getConfig().getCurrentCallDepth());
            outputs = mComponentConnector.findInputInterfaces(outputs, bbm);
            Collection<IN> inputs = interComponentAnalysis(outputs);
            outputs = null;
            outputs = intraComponentAnalysis(inputs);
            inputs = null;
        }
    }

    /**
     * The analysis within the component black-boxes (Phase 3)
     * @param inputs    the current component black-box {@link InputInterface}s
     * @return          the updated directly reachable {@link OutputInterface}s
     */
    protected abstract Collection<OUT> intraComponentAnalysis(Collection<IN> inputs);
    
    /**
     * The analysis step between component black-boxes (Phase 3)
     * @param outputs       the current component black-box {@link OutputInterface}
     * @return              the updated directly reachable {@link InputInterface}s
     */
    protected abstract Collection<IN> interComponentAnalysis(Collection<OUT> outputs);
    
    /**
     * Update the security label of an {@link BlackBoxElement} with the given security labels
     * @param elem          the {@link BlackBoxElement} to update
     * @param secLabels     the {@link SecurityLabel}s
     */
    protected void updateSecurityLabel(BlackBoxElement elem, Set<SecurityLabel> secLabels) {
        elem.updateSecurityLabels(secLabels);
    }
    
    /**
     * Check the graph node for security conflicts
     * @param node      the graph node to check
     */
    protected void detectConflict(NODE node) {
        if (mConflictDetector != null) {
            mConflictDetector.checkForConflict(node);
        }
    }
}
