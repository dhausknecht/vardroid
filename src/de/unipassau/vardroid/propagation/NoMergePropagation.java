/**
 * 
 */
package de.unipassau.vardroid.propagation;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import de.unipassau.vardroid.blackbox.SecurityLabel;
import de.unipassau.vardroid.blackbox.element.BlackBoxElement;
import de.unipassau.vardroid.blackbox.element.DataSink;
import de.unipassau.vardroid.blackbox.element.DataSource;
import de.unipassau.vardroid.blackbox.element.InputInterface;
import de.unipassau.vardroid.blackbox.element.InternalSink;
import de.unipassau.vardroid.blackbox.element.OutputInterface;
import de.unipassau.vardroid.connector.ComponentConnector;
import de.unipassau.vardroid.graph.Graph;

/**
 * Propagate {@link SecurityLabel}s without any merging.
 *
 * @author Daniel Hausknecht
 */
public class NoMergePropagation extends PropagationHandler<InputInterface, OutputInterface, BlackBoxElement> {

    /**
     * {@inheritDoc}
     */
    public NoMergePropagation(ComponentConnector<InputInterface, OutputInterface> compConn, Graph<BlackBoxElement> graph) {
        super(compConn, graph);
    }

    @Override
    protected Collection<OutputInterface> intraComponentAnalysis(Collection<InputInterface> inputs) {
        // find all data sources
        Set<DataSource> dataSources = new HashSet<DataSource>();
        for (InputInterface in : inputs) {
            dataSources.addAll(in.getComponent().getInternalSources());
        }
        getGraph().insertAll(dataSources);
        dataSources.addAll(inputs);
        
        // update security labels
        for (DataSource src : dataSources) {
            Set<DataSink> dataSinks = new HashSet<DataSink>(src.getSinks());
            for (DataSink sink : dataSinks) {
                updateSecurityLabel(sink, src.getSecurityLabels());
                detectConflict(sink);
            }
        }

        Set<OutputInterface> outputs = new HashSet<OutputInterface>();
        Set<InternalSink> internalSinks = new HashSet<InternalSink>();
        for (DataSource src : dataSources) {
            for (DataSink sink : src.getSinks()) {
                if (sink instanceof InternalSink) {
                    internalSinks.add((InternalSink) sink);
                } else if (sink instanceof OutputInterface) {
                    outputs.add((OutputInterface) sink);
                }
            }
        }
        getGraph().insertAll(internalSinks);
        getGraph().insertAll(outputs);
        
        return outputs;
    }

    @Override
    protected Collection<InputInterface> interComponentAnalysis(Collection<OutputInterface> outputs) {
        Collection<InputInterface> result = new HashSet<InputInterface>();
        
        // merge globally and insert input-interfaces into graph
        for (OutputInterface out : outputs) {
            for (InputInterface in : out.getTargetInputInterfaces()) {
                updateSecurityLabel(in, out.getSecurityLabels());
                detectConflict(in);
                result.add(in);
            }
        }
        getGraph().insertAll(result);
        
        return result;
    }

}
