package de.unipassau.vardroid.propagation;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import de.unipassau.vardroid.blackbox.element.BlackBoxElement;
import de.unipassau.vardroid.blackbox.element.DataSink;
import de.unipassau.vardroid.blackbox.element.DataSource;
import de.unipassau.vardroid.blackbox.element.InputInterface;
import de.unipassau.vardroid.blackbox.element.InternalSink;
import de.unipassau.vardroid.blackbox.element.InternalSource;
import de.unipassau.vardroid.blackbox.element.OutputInterface;
import de.unipassau.vardroid.connector.ComponentConnector;
import de.unipassau.vardroid.graph.Graph;
import de.unipassau.vardroid.state.State;

/**
 * Propagate security labels while merging states globally.
 *
 * @author Daniel Hausknecht
 */
public class VarawareGlobalMergePropagation
        extends PropagationHandler<State<InputInterface>, State<OutputInterface>, State<? extends BlackBoxElement>> {

    public VarawareGlobalMergePropagation(ComponentConnector<State<InputInterface>, State<OutputInterface>> compConn,
            Graph<State<? extends BlackBoxElement>> graph) {
        super(compConn, graph);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Collection<State<OutputInterface>> intraComponentAnalysis(Collection<State<InputInterface>> inputs) {
        // find all sources and create source states
        // security label hash -> data source state
        Map<Integer, State<InternalSource>> internalSourceStates = new HashMap<Integer, State<InternalSource>>();
        for (State<InputInterface> state : inputs) {
            for (InputInterface in : state.getMergedElements()) {
                for (InternalSource is : in.getComponent().getInternalSources()) {
                    State<InternalSource> isState = internalSourceStates.get(is.getSecurityLabelHash());
                    
                    if (isState == null) {
                        isState = new State<InternalSource>(is, is.getComponent().getCallDepth());
                        internalSourceStates.put(isState.getSecurityLabelHash(), isState);
                        
                    } else {
                        InternalSource mergedSrc = isState.getMergedElement(is.hashCode());
                        if (mergedSrc == null) {
                            isState.addMergedElement(is);
                        } else {
                            mergedSrc = mergedSrc.mergeWith(is);
                        }
                    }
                }
            }
        }
        
        Stack<State<? extends DataSource>> sourceStateStack = new Stack<State<? extends DataSource>>();
        sourceStateStack.addAll(internalSourceStates.values());
        sourceStateStack.addAll(inputs);
        
        // propagate security labels
        // element instances might change throughout the propagation process, so do not immediately merge them but 
        // store them in the same bucket (labelled with the element hash). The instances are then distinguished by 
        // their component instance ID. The internalSinkMap and outputMap semantics are:
        // element hash -> ( component id -> element instance )
        Map<Integer, Map<Integer, InternalSink>> internalSinkMap = new HashMap<Integer, Map<Integer, InternalSink>>();
        Map<Integer, Map<Integer, OutputInterface>> outputMap = new HashMap<Integer, Map<Integer, OutputInterface>>();
        for (State<? extends DataSource> srcState : sourceStateStack) {
            for (DataSource src : srcState.getMergedElements()) {
                for (DataSink sink : src.getSinks()) {
                    if (sink instanceof InternalSink) {
                        Map<Integer, InternalSink> elemMap = internalSinkMap.get(sink.hashCode());
                        if (elemMap != null) {
                            elemMap.remove(sink.getComponent().getId().hashCode());
                        }
                        
                        updateSecurityLabel(sink, src.getSecurityLabels());
                        
                        elemMap = internalSinkMap.get(sink.hashCode());
                        if (elemMap == null) {
                            elemMap = new HashMap<Integer, InternalSink>();
                            internalSinkMap.put(sink.hashCode(), elemMap);
                        }
                        // component.id contains instance identifier
                        elemMap.put(sink.getComponent().getId().hashCode(), (InternalSink) sink);
                        
                    } else if (sink instanceof OutputInterface) {
                        Map<Integer, OutputInterface> elemMap = outputMap.get(sink.hashCode());
                        if (elemMap != null) {
                            elemMap.remove(sink.getComponent().getId().hashCode());
                        }
                        
                        updateSecurityLabel(sink, src.getSecurityLabels());
                        
                        elemMap = outputMap.get(sink.hashCode());
                        if (elemMap == null) {
                            elemMap = new HashMap<Integer, OutputInterface>();
                            outputMap.put(sink.hashCode(), elemMap);
                        }
                        // component.id contains instance identifier
                        elemMap.put(sink.getComponent().getId().hashCode(), (OutputInterface) sink);
                    }
                }
            }
        }
        
        // merge data sinks in "container maps"
        Map<Integer, InternalSink> mergedInternalSinks = new HashMap<Integer, InternalSink>();
        for (Integer elemHash : internalSinkMap.keySet()) {
            Map<Integer, InternalSink> elemMap = internalSinkMap.get(elemHash);
            if (!elemMap.isEmpty()) {
                InternalSink mergedIs = null;
                for (Integer id : elemMap.keySet()) {
                    mergedIs = elemMap.get(id);
                    break;
                }
                mergedInternalSinks.put(elemHash, mergedIs);
            }
        }
        internalSinkMap = null;
        Map<Integer, OutputInterface> mergedOutputs = new HashMap<Integer, OutputInterface>();
        for (Integer elemHash : outputMap.keySet()) {
            Map<Integer, OutputInterface> elemMap = outputMap.get(elemHash);
            if (!elemMap.isEmpty()) {
                OutputInterface mergedOutput = null;
                for (Integer id : elemMap.keySet()) {
                    mergedOutput = elemMap.get(id);
                    break;
                }
                mergedOutputs.put(elemHash, mergedOutput);
            }
        }
        outputMap = null;

        // create states
        // states under constant change, so do not identify them by their hash. Also, there can be only 
        // a single state per security label. So, there is no need to go for the hash value.
        // security label hash -> state
        Map<Integer, State<InternalSink>> internalSinkStates = new HashMap<Integer, State<InternalSink>>();
        Map<Integer, State<OutputInterface>> outputStates = new HashMap<Integer, State<OutputInterface>>();
        for (State<? extends DataSource> srcState : sourceStateStack) {
            for (DataSource src : srcState.getMergedElements()) {
                List<DataSink> sinkList = new LinkedList<DataSink>(src.getSinks());
                for (DataSink sink : sinkList) {
                    src.removeSuccessor(sink);
                    
                    if (sink instanceof InternalSink) {
                        InternalSink mergedSink = mergedInternalSinks.get(sink.hashCode());
                        src.addSuccessor(mergedSink);
                        
                        State<InternalSink> sinkState = internalSinkStates.get(mergedSink.getSecurityLabelHash());
                        if (sinkState == null) {
                            sinkState = new State<InternalSink>(mergedSink, mergedSink.getComponent().getCallDepth());
                            internalSinkStates.put(sinkState.getSecurityLabelHash(), sinkState);
                            srcState.addSuccessor(sinkState, mergedSink);
                            
                        } else {
                            sinkState.addMergedElement(mergedSink);
                            srcState.addSuccessor(sinkState, mergedSink);
                        }
                        
                    } else if (sink instanceof OutputInterface) {
                        OutputInterface mergedOut = mergedOutputs.get(sink.hashCode());
                        src.addSuccessor(mergedOut);
                        
                        State<OutputInterface> outState = outputStates.get(mergedOut.getSecurityLabelHash());
                        if (outState == null) {
                            outState = new State<OutputInterface>(mergedOut, mergedOut.getComponent().getCallDepth());
                            outputStates.put(outState.getSecurityLabelHash(), outState);
                            srcState.addSuccessor(outState, mergedOut);
                            
                        } else {
                            outState.addMergedElement(mergedOut);
                            srcState.addSuccessor(outState, mergedOut);
                        }
                    }
                }
            }
        }
        
        // merge sink states globally and update successors accordingly
        Set<State<OutputInterface>> resultSet = new HashSet<State<OutputInterface>>();
        while (!sourceStateStack.isEmpty()) {
            State<? extends DataSource> srcState= sourceStateStack.pop();
            List<BlackBoxElement> succElemList = new LinkedList<BlackBoxElement>(srcState.getSuccessors().keySet());
            for (BlackBoxElement succElem : succElemList) {
                State<? extends BlackBoxElement> succState = srcState.getSuccessors().get(succElem);
                State<? extends BlackBoxElement> globalState = getGraph().getNode(succState);
                if (globalState == null) {
                    getGraph().insert(succState);
                    detectConflict(succState);
                    if (succElem instanceof OutputInterface) {
                        resultSet.add((State<OutputInterface>) succState);
                    }
                } else {
                    srcState.getSuccessors().remove(succElem);
                    srcState.addSuccessor(globalState, globalState.getMergedElement(succElem.hashCode()));
                    DataSink succSink = (DataSink) succElem;
                    for (DataSource src : srcState.getMergedElements()) {
                        if (src.hasSuccessor(succSink)) {
                            src.removeSuccessor(succSink);
                            src.addSuccessor((DataSink) globalState.getMergedElement(succElem.hashCode()));
                        }
                    }
                }
            }
        }
        sourceStateStack = null;

        // put internal data sources into global state map and merge successors
        for (State<InternalSource> internalState : internalSourceStates.values()) {
            State<? extends BlackBoxElement> globalState = getGraph().getNode(internalState);
            if (globalState == null) {
                getGraph().insert(internalState);
                // we assume internal sources to be free of conflicts
            } else {
                for (BlackBoxElement succElem : internalState.getSuccessors().keySet()) {
                    if (!globalState.getSuccessors().containsKey(succElem)) {
                        globalState.addSuccessor(internalState.getSuccessors().get(succElem), succElem);
                    }
                }
            }
        }

        return resultSet;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Collection<State<InputInterface>> interComponentAnalysis(Collection<State<OutputInterface>> outputs) {
        Map<Integer, State<InputInterface>> inputStates = new HashMap<Integer, State<InputInterface>>();
        
        for (State<OutputInterface> outState : outputs) {
            for (OutputInterface out : outState.getMergedElements()) {
                List<InputInterface> inputList = new LinkedList<InputInterface>(out.getTargetInputInterfaces());
                for (InputInterface in : inputList) {
                    updateSecurityLabel(in, out.getSecurityLabels());
                    
                    State<InputInterface> inState = inputStates.get(in.getSecurityLabelHash());
                    if (inState == null) {
                        inState = new State<InputInterface>(in, in.getComponent().getCallDepth());
                        inputStates.put(inState.getSecurityLabelHash(), inState);
                        outState.addSuccessor(inState, in);
                        
                    } else {
                        InputInterface mergedInput = inState.getMergedElement(in.hashCode());
                        if (mergedInput == null) {
                            inState.addMergedElement(in);
                            outState.addSuccessor(inState, in);
                            
                        } else {
                            out.removeTargetInputInterface(in);
                            out.addTargetInputInterface(mergedInput);
                            outState.addSuccessor(inState, mergedInput);
                        }
                    }
                }
            }
        }
        
        // merge input states globally and update successors accordingly
        Set<State<InputInterface>> resultSet = new HashSet<State<InputInterface>>();
        for (State<OutputInterface> outState : outputs) {
            List<BlackBoxElement> succElemList = new LinkedList<BlackBoxElement>(outState.getSuccessors().keySet());
            for (BlackBoxElement succElem : succElemList) {
                State<? extends BlackBoxElement> succState = outState.getSuccessors().get(succElem);
                State<? extends BlackBoxElement> globalState = getGraph().getNode(succState);
                if (globalState == null) {
                    getGraph().insert(succState);
                    detectConflict(succState);
                    resultSet.add((State<InputInterface>) succState);
                } else {
                    outState.getSuccessors().remove(succElem);
                    outState.addSuccessor(globalState, globalState.getMergedElement(succElem.hashCode()));
                    InputInterface succInput = (InputInterface) succElem;
                    for (OutputInterface out : outState.getMergedElements()) {
                        if (out.getTargetInputInterfaces().contains(succInput)) {
                            out.removeTargetInputInterface(succInput);
                            out.addTargetInputInterface((InputInterface) globalState.getMergedElement(succElem.hashCode()));
                        }
                    }
                }
            }
        }

        return resultSet;
    }

}
