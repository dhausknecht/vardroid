/**
 * 
 */
package de.unipassau.vardroid.propagation;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import de.unipassau.vardroid.blackbox.element.BlackBoxElement;
import de.unipassau.vardroid.blackbox.element.DataSink;
import de.unipassau.vardroid.blackbox.element.DataSource;
import de.unipassau.vardroid.blackbox.element.InputInterface;
import de.unipassau.vardroid.blackbox.element.InternalSink;
import de.unipassau.vardroid.blackbox.element.InternalSource;
import de.unipassau.vardroid.blackbox.element.OutputInterface;
import de.unipassau.vardroid.connector.ComponentConnector;
import de.unipassau.vardroid.graph.Graph;

/**
 * Propagate security labels while merging component black boxes per level.
 *
 * @author Daniel Hausknecht
 */
public class LevelMergePropagation extends PropagationHandler<InputInterface, OutputInterface, BlackBoxElement> {

    public LevelMergePropagation(ComponentConnector<InputInterface, OutputInterface> compConn, Graph<BlackBoxElement> graph) {
        super(compConn, graph);
    }

    @Override
    protected Collection<OutputInterface> intraComponentAnalysis(Collection<InputInterface> inputs) {
        // find all internal sources
        Map<Integer, InternalSource> internalSources = new HashMap<Integer, InternalSource>();
        for (InputInterface in : inputs) {
            for (InternalSource is : in.getComponent().getInternalSources()) {
                InternalSource mergedIs = internalSources.get(is.hashCode());
                if (mergedIs == null) {
                    internalSources.put(is.hashCode(), is);
                } else {
                    mergedIs = mergedIs.mergeWith(is);
                }
            }
        }
        Stack<DataSource> sourceStateStack = new Stack<DataSource>();
        sourceStateStack.addAll(internalSources.values());
        sourceStateStack.addAll(inputs);
        
        // propagate security labels
        Map<Integer, Map<Integer, InternalSink>> internalSinkMap = new HashMap<Integer, Map<Integer, InternalSink>>();
        Map<Integer, Map<Integer, OutputInterface>> outputMap = new HashMap<Integer, Map<Integer, OutputInterface>>();
        for (DataSource src : sourceStateStack) {
            for (DataSink sink : src.getSinks()) {
                if (sink instanceof InternalSink) {
                    Map<Integer, InternalSink> elemMap = internalSinkMap.get(sink.hashCode());
                    if (elemMap != null) {
                        elemMap.remove(sink.getComponent().getId().hashCode());
                    }
                    
                    updateSecurityLabel(sink, src.getSecurityLabels());
                    
                    elemMap = internalSinkMap.get(sink.hashCode());
                    if (elemMap == null) {
                        elemMap = new HashMap<Integer, InternalSink>();
                        internalSinkMap.put(sink.hashCode(), elemMap);
                    }
                    // component.id contains instance identifier
                    elemMap.put(sink.getComponent().getId().hashCode(), (InternalSink) sink);
                    
                } else if (sink instanceof OutputInterface) {
                    Map<Integer, OutputInterface> elemMap = outputMap.get(sink.hashCode());
                    if (elemMap != null) {
                        elemMap.remove(sink.getComponent().getId().hashCode());
                    }
                    
                    updateSecurityLabel(sink, src.getSecurityLabels());
                    
                    elemMap = outputMap.get(sink.hashCode());
                    if (elemMap == null) {
                        elemMap = new HashMap<Integer, OutputInterface>();
                        outputMap.put(sink.hashCode(), elemMap);
                    }
                    // component.id contains instance identifier
                    elemMap.put(sink.getComponent().getId().hashCode(), (OutputInterface) sink);
                }
            }
        }
        
        // merge data sinks in "container maps"
        Map<Integer, InternalSink> mergedInternalSinks = new HashMap<Integer, InternalSink>();
        for (Integer elemHash : internalSinkMap.keySet()) {
            Map<Integer, InternalSink> elemMap = internalSinkMap.get(elemHash);
            if (!elemMap.isEmpty()) {
                InternalSink mergedIs = null;
                for (Integer id : elemMap.keySet()) {
                    mergedIs = elemMap.get(id);
                    break;
                }
                mergedInternalSinks.put(elemHash, mergedIs);
            }
        }
        internalSinkMap = null;
        Map<Integer, OutputInterface> mergedOutputs = new HashMap<Integer, OutputInterface>();
        for (Integer elemHash : outputMap.keySet()) {
            Map<Integer, OutputInterface> elemMap = outputMap.get(elemHash);
            if (!elemMap.isEmpty()) {
                OutputInterface mergedOutput = null;
                for (Integer id : elemMap.keySet()) {
                    mergedOutput = elemMap.get(id);
                    break;
                }
                mergedOutputs.put(elemHash, mergedOutput);
            }
        }
        outputMap = null;

        // replace successors with merged ones and insert data sinks into graph
        List<OutputInterface> outputs = new LinkedList<OutputInterface>();
        while (!sourceStateStack.isEmpty()) {
            DataSource src = sourceStateStack.pop();
            List<DataSink> sinkList = new LinkedList<DataSink>(src.getSinks());
            for (DataSink sink : sinkList) {
                src.removeSuccessor(sink);
                
                if (sink instanceof InternalSink) {
                    InternalSink globalSink = (InternalSink) getGraph().getNode(sink);
                    if (globalSink == null) {
                        InternalSink mergedSink = mergedInternalSinks.get(sink.hashCode());
                        src.addSuccessor(mergedSink);
                        getGraph().insert(mergedSink);
                        detectConflict(mergedSink);
                    } else {
                        src.addSuccessor(globalSink);
                    }
                    
                } else if (sink instanceof OutputInterface) {
                    OutputInterface globalOut = (OutputInterface) getGraph().getNode(sink);
                    if (globalOut == null) {
                        OutputInterface mergedOut = mergedOutputs.get(sink.hashCode());
                        src.addSuccessor(mergedOut);
                        getGraph().insert(mergedOut);
                        detectConflict(mergedOut);
                        outputs.add(mergedOut);
                    } else {
                        src.addSuccessor(globalOut);
                    }
                }
            }
        }
        sourceStateStack = null;

        // insert internal data sources into graph and merge successors
        for (InternalSource internalSrc : internalSources.values()) {
            InternalSource globalSrc = (InternalSource) getGraph().getNode(internalSrc);
            if (globalSrc == null) {
                getGraph().insert(internalSrc);
                // we assume internal sources to be free of conflicts
            } else {
                for (DataSink succElem : internalSrc.getSinks()) {
                    if (!globalSrc.getSinks().contains(succElem)) {
                        globalSrc.addSuccessor(succElem);
                    }
                }
            }
        }
        
        return outputs;
    }

    @Override
    protected Collection<InputInterface> interComponentAnalysis(Collection<OutputInterface> outputs) {
        Collection<InputInterface> result = new LinkedList<InputInterface>();
        
        // merge globally and insert input-interfaces into graph
        List<OutputInterface> outputList = new LinkedList<OutputInterface>(outputs);
        for (OutputInterface out : outputList) {
            List<InputInterface> inputList = new LinkedList<InputInterface>(out.getTargetInputInterfaces());
            for (InputInterface in : inputList) {
                updateSecurityLabel(in, out.getSecurityLabels());
                
                InputInterface mergedInput = (InputInterface) getGraph().getNode(in);
                if (mergedInput == null) {
                    result.add(in);
                    getGraph().insert(in);
                    detectConflict(in);
                } else {
                    out.removeTargetInputInterface(in);
                    out.addTargetInputInterface(mergedInput);
                }
            }
        }
        
        return result;
    }

}
