package de.unipassau.vardroid;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import de.unipassau.vardroid.log.GraphvizWriter;
import de.unipassau.vardroid.log.Log;


/**
 * Container for analysis execution configuration.
 *
 * @author Daniel Hausknecht
 */
public final class Config {
    public static final int INPUT_MODE_TEST = 0;
    public static final int INPUT_MODE_XML = 1;

    public static final int MERGE_MODE_NONE = 0;
    public static final int MERGE_MODE_ELEM_LEVEL = 1;
    public static final int MERGE_MODE_ELEM_GLOBAL = 2;
    public static final int MERGE_MODE_STATE_LEVEL = 3;
    public static final int MERGE_MODE_STATE_GLOBAL = 4;
    
    public static final int SUCC_MODE_SEPARATE = 0;
    public static final int SUCC_MODE_COMBINED = 1;
    
    private static final String OUTPUT_BASE_DIR = System.getProperty("user.dir") + File.separator + "output";
    public static final String DOT_PHASE2_PATH = OUTPUT_BASE_DIR + File.separator + "phase2.dot";
    public static final String DOT_PHASE3_PATH = OUTPUT_BASE_DIR + File.separator + "phase3.dot";
    public static final String DOT_PHASE3_STATE_PATH = OUTPUT_BASE_DIR + File.separator + "phase3_state.dot";
    
    private static Config sConfig;
    
    private int mInputMode;
    private int mMergeMode;
    private int mCurrentCallDepth;
    private int mMaxAnalysisDepth;
    private boolean hasTransitionLabels;
    private int mFindSuccMode;
    private boolean mPrintDebugLog;
    private boolean mPrintWarningLog;
    private boolean mPrintErrorLog;
    private boolean mPrintDotFile;
    private String mComponentXMLFilePath;
    private String mConflictsXMLFilePath;
//    private String mDbName;
    
    private Map<String, GraphvizWriter> mGraphvizWriterMap;
    
    private Config() {
        // set some default values
        mInputMode = INPUT_MODE_TEST;
        mMergeMode = MERGE_MODE_NONE;
        mCurrentCallDepth = 0;
        mMaxAnalysisDepth = 4;
        hasTransitionLabels = true;
        mFindSuccMode = SUCC_MODE_SEPARATE;
        mPrintDebugLog = true;
        mPrintWarningLog = true;
        mPrintErrorLog = true;
        mPrintDotFile = false;
        mComponentXMLFilePath = null;
        mConflictsXMLFilePath = null;
        
        mGraphvizWriterMap = new HashMap<String, GraphvizWriter>();
        if (!(new File(OUTPUT_BASE_DIR).exists())) {
            new File(OUTPUT_BASE_DIR).mkdir();
        }
        initGraphvizWriter(DOT_PHASE2_PATH);
        initGraphvizWriter(DOT_PHASE3_PATH);
        initGraphvizWriter(DOT_PHASE3_STATE_PATH);
    }
    
    /**
     * Parses the program arguments and set the runtime configurations.
     * @param args      the program arguments
     * @return          the configuration object
     */
    public static Config getConfig(String[] args) {
        if (!parseFile(args[0])) {
            System.exit(0);
        }
        
        if ((3 <= args.length) && (args[1]).equals("-input")) {
            sConfig.setComponentXMLFilePath(args[2]);
        }
        return sConfig;
    }
    
    /**
     * Parse the configuration file
     * @param path      the file location
     * @return          <code>true</code> if no parsing error occurs, <code>false</code> otherwise
     */
    public static boolean parseFile(String path) {
        sConfig = new Config();
        
        Log.d("Parsing config file...");
        
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
        } catch (FileNotFoundException e) {
            Log.e(e.getMessage());
            return false;
        }
        String line = null;
        do {
            try {
                line = br.readLine();
            } catch (IOException e) {
                Log.e(e.getMessage());
                try {
                    br.close();
                } catch (IOException e1) {
                    Log.e(e1.getMessage());
                    return false;
                }
                return false;
            }
            if ((line != null) && (!parseLine(line))) {
                try {
                    br.close();
                } catch (IOException e) {
                    Log.e(e.getMessage());
                    return false;
                }
                return false;
            }
        } while (line != null);

        try {
            br.close();
        } catch (IOException e) {
            Log.e(e.getMessage());
            return false;
        }
        Log.d("parsing successful");
        return true;
    }
    
    private static boolean parseLine(String line) {
        if (line.matches("")) {
            // do nothing on empty lines
            
        } else if (line.matches("input\\s+=\\s+.+")) {
            sConfig.setInputMode(INPUT_MODE_XML);
            sConfig.setComponentXMLFilePath(line.split("input\\s+=\\s+")[1]);
            
        } else if (line.matches("conflicts\\s+=\\s+.+")) {
            sConfig.setConflictsXMLFilePath(line.split("conflicts\\s+=\\s+")[1]);
            
//        } else if (line.matches("db\\s+=\\s+.+")) {
//            sConfig.setDbName(line.split("db\\s+=\\s+")[1]);
            
        } else if (line.matches("log\\s+=\\s+(none|[dew]+)")) {
            String mode = line.split("log\\s+=\\s+")[1];
            if (!mode.equals("none")) {
                for (char c : mode.toCharArray()) {
                    switch (c) {
                    case 'd':
                        sConfig.setDebugLog(true);
                        break;
                    case 'w':
                        sConfig.setWarningLog(true);
                        break;
                    case 'e':
                        sConfig.setErrorLog(true);
                        break;
                    default:
                        Log.e("Unknown logging mode \"" + mode + "\"");
                        return false;
                    }
                }
            }
        } else if (line.matches("callDepth\\s+=\\s+\\d+")) {
            sConfig.setMaxAnalysisDepth(Integer.valueOf(line.split("callDepth\\s+=\\s+")[1]));
            
        } else if (line.matches("findSuccessor\\s+=\\s+[a-z]+")) {
            String mode = line.split("findSuccessor\\s+=\\s+")[1];
            if (mode.equals("separate")) {
                sConfig.setSuccessorSearchMode(SUCC_MODE_SEPARATE);
            } else if (mode.equals("combined")) {
                sConfig.setSuccessorSearchMode(SUCC_MODE_COMBINED);
            } else {
                Log.e("Unknown mode for successor search \"findSuccessorMode\"");
                return false;
            }
            
        } else if (line.matches("dot\\s+=\\s+\\d+")) {
            sConfig.setPrintDotFile(Integer.valueOf(line.split("dot\\s+=\\s+")[1]) != 0);
            
        } else if (line.matches("mergeMode\\s+=\\s+.+")) {
            String mode = line.split("mergeMode\\s+=\\s+")[1];
            if (mode.equals("none")) {
                sConfig.mMergeMode = MERGE_MODE_NONE;
            } else if (mode.equals("element-level")) {
                sConfig.mMergeMode = MERGE_MODE_ELEM_LEVEL;
            } else if (mode.equals("element-global")) {
                sConfig.mMergeMode = MERGE_MODE_ELEM_GLOBAL;
            } else if (mode.equals("state-level")) {
                sConfig.mMergeMode = MERGE_MODE_STATE_LEVEL;
            } else if (mode.equals("state-global")) {
                sConfig.mMergeMode = MERGE_MODE_STATE_GLOBAL;
            } else {
                Log.e("Unknown state merge mode!");
                return false;
            }
            
        } else if (line.matches("labels\\s+=\\s+\\d+")) {
            sConfig.hasTransitionLabels = (Integer.valueOf(line.split("labels\\s+=\\s+")[1]) != 0);
            
        } else {
            Log.w("Unknown flag " + line);
            return false;
        }
        return true;
    }
    
    public static Config getConfig() {
        if (sConfig == null) {
            sConfig = new Config();
        }
        return sConfig;
    }

    public int getInputMode() {
        return mInputMode;
    }

    public int getMergeMode() {
        return mMergeMode;
    }

    private void setInputMode(int mInputMode) {
        this.mInputMode = mInputMode;
    }
    
    public void increaseCurrentCallDepth() {
        mCurrentCallDepth++;
    }
    
    public void resetCurrentCallDepth() {
        mCurrentCallDepth = 0;
    }
    
    public int getCurrentCallDepth() {
        return mCurrentCallDepth;
    }

    public int getMaxAnalysisDepth() {
        return mMaxAnalysisDepth;
    }

    private void setMaxAnalysisDepth(int maxDepth) {
        mMaxAnalysisDepth = maxDepth;
    }
    
    public boolean hasTransitionLabels() {
        return hasTransitionLabels;
    }
    
    private void setSuccessorSearchMode(int mode) {
        mFindSuccMode = mode;
    }
    
    public int getSuccessorSearchMode() {
        return mFindSuccMode;
    }

    private void setDebugLog(boolean enabled) {
        mPrintDebugLog = enabled;
    }
    
    public boolean printDebugLog() {
        return mPrintDebugLog;
    }

    private void setWarningLog(boolean enabled) {
        mPrintWarningLog = enabled;
    }
    
    public boolean printWarningLog() {
        return mPrintWarningLog;
    }

    private void setErrorLog(boolean enabled) {
        mPrintErrorLog = enabled;
    }
    
    public boolean printErrorLog() {
        return mPrintErrorLog;
    }
    
    private void setPrintDotFile(boolean enable) {
        mPrintDotFile = enable;
    }
    
    public boolean printDotFile() {
        return mPrintDotFile;
    }
    
    private void setComponentXMLFilePath(String path) {
        mComponentXMLFilePath = path;
    }
    
    public String getComponentXMLFilePath() {
        return mComponentXMLFilePath;
    }
    
    private void setConflictsXMLFilePath(String path) {
        mConflictsXMLFilePath = path;
    }
    
    public String getConflictsXMLFilePath() {
        return mConflictsXMLFilePath;
    }
    
//    private void setDbName(String name) {
//        mDbName = name;
//    }
    
//    public String getDbName() {
//        return mDbName;
//    }

    public void initGraphvizWriter(String filePath) {
        mGraphvizWriterMap.put(filePath, new GraphvizWriter(filePath));
    }
    
    public GraphvizWriter getGraphvizWriter(String filePath) {
        return mGraphvizWriterMap.get(filePath);
    }
    
    public void finaliseGraphvizWriter(String filePath) {
        GraphvizWriter gw = mGraphvizWriterMap.get(filePath);
        if (gw != null) { gw.closeFile(); }
    }
}
